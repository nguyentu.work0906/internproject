<?php
/*
Template Name: Vietnam Page
*/
get_header();
?>

<div class="main-wrapper">
    <div class="main-container">
        <div class="page-wrapper">
            <div class="sections-container">
                <script type="text/javascript">
                    UNCODE.initHeader();
                </script>
                <article id="post-1451" class="page-body style-light-bg post-1451 page type-page status-publish hentry page_category-reportage">
                    <div class="post-wrapper">
                        <div class="post-body">
                            <div class="post-content un-no-sidebar-layout">
                                <div data-parent="true" class="vc_row row-container boomapps_vcrow">
                                    <div class="row limit-width row-parent">
                                        <div class="wpb_row row-inner">
                                            <div class="wpb_column pos-top pos-center align_left column_parent col-lg-12 boomapps_vccolumn one-internal-gutter">
                                                <div class="uncol style-light">
                                                    <div class="uncoltable">
                                                        <div class="uncell  boomapps_vccolumn no-block-padding">
                                                            <div class="uncont">
                                                                <div class="vc_custom_heading_wrap ">
                                                                    <div class="heading-text el-text">
                                                                        <h2 class="h6">
                                                                            <span>Travel</span>
                                                                        </h2>
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="vc_custom_heading_wrap ">
                                                                    <div class="heading-text el-text">
                                                                        <h3 class="h2"><span>Vietnam</span>
                                                                        </h3>
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <script id="script-110309" data-row="script-110309" type="text/javascript" class="vc_controls">
                                                UNCODE.initRow(document.getElementById("script-110309"));
                                            </script>
                                        </div>
                                    </div>
                                </div>
                                <div data-parent="true" class="vc_row row-container boomapps_vcrow">
                                    <div class="row limit-width row-parent">
                                        <div class="wpb_row row-inner">
                                            <div class="wpb_column pos-top pos-center align_left column_parent col-lg-12 boomapps_vccolumn single-internal-gutter">
                                                <div class="uncol style-light">
                                                    <div class="uncoltable">
                                                        <div class="uncell  boomapps_vccolumn no-block-padding">
                                                            <div class="uncont">
                                                                <div id="gallery-153226" class="isotope-system">
                                                                    <div class="isotope-wrapper single-gutter">
                                                                        <div class="isotope-container isotope-layout style-masonry" data-type="masonry" data-layout="masonry" data-lg="1000" data-md="600" data-sm="480">
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-295  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="http://blog.test/wp-content/uploads/2024/05/faker-halloffame-lmht-4-17163932085711708666827.webp" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'http://blog.test/wp-content/uploads/2024/05/faker-halloffame-lmht-4-17163932085711708666827.webp'" data-lb-index="0">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="http://blog.test/wp-content/uploads/2024/05/faker-halloffame-lmht-4-17163932085711708666827.webp" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-294  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="http://blog.test/wp-content/uploads/2024/05/man-utd-14-2505233355.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'http://blog.test/wp-content/uploads/2024/05/man-utd-14-2505233355.jpg'" data-lb-index="1">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="http://blog.test/wp-content/uploads/2024/05/man-utd-14-2505233355.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-293  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-13.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-13-600x400.jpg'" data-lb-index="2">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-13-uai-720x480.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-299  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-19.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-19-600x400.jpg'" data-lb-index="3">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-19-uai-720x480.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-298  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-18.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-18-600x400.jpg'" data-lb-index="4">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-18-uai-720x480.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-292  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-12.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-12-600x400.jpg'" data-lb-index="5">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-12-uai-720x480.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-286  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-6.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-6-600x400.jpg'" data-lb-index="6">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-6-uai-720x480.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-287  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-7.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-7-600x400.jpg'" data-lb-index="7">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-7-uai-720x480.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-288  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-8.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-8-600x400.jpg'" data-lb-index="8">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-8-uai-720x480.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-289  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-9.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-9-600x400.jpg'" data-lb-index="9">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-9-uai-720x480.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-296  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-16.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-16-600x400.jpg'" data-lb-index="10">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-16-uai-720x480.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-iso-w4 tmb-iso-h4 tmb-light tmb-overlay-text-anim tmb-overlay-middle tmb-overlay-text-left tmb-bordered tmb-id-290  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg tmb-lightbox">
                                                                                <div class="t-inside">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 66.7%;">
                                                                                                </div>
                                                                                                <a tabindex="-1" href="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-10.jpg" class="pushed" data-skin="white" data-lbox="ilightbox_gallery-153226" data-options="width:900,height:600,thumbnail: 'https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-10-600x400.jpg'" data-lb-index="11">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-dark-bg" style="opacity: 0.01;">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/Fulvio_Bugani_Shinta_Ratri-10-uai-720x480.jpg" width="720" height="480" alt="reportage Bugani Transgender Waria" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <!-- <div class="uncode_text_column vc_custom_1594645810434">
                                                                                <p>What does it mean to be a transgender
                                                                                    and a muslim?<br /> This is a
                                                                                    complicated matter especially in a
                                                                                    religion which has such a strict
                                                                                    division between genders.<br />
                                                                                    Generally
                                                                                    speaking Islamic law forbids men to
                                                                                    dress and adopt the mannerisms of
                                                                                    women, and vice versa, but according
                                                                                    to the Koran, muslims are not
                                                                                    allowed to classify people based
                                                                                    on economic, social, political,
                                                                                    gender or theological values.<br />
                                                                                    Indonesia has the largest Muslim
                                                                                    population in the world, with
                                                                                    approximately 202.9 million
                                                                                    identifying
                                                                                    themselves as Muslim (87.2% of
                                                                                    Indonesia&#8217;s total population
                                                                                    and 13,1% of the World Muslim
                                                                                    population)<br /> Here transsexuals
                                                                                    are known as waria, a term which is
                                                                                    a combination of two Indonesian
                                                                                    words:  “wanita,” which means woman,
                                                                                    and “pria,” which means man.<br />
                                                                                    The area of greater Yogyakarta,
                                                                                    located on the island of Java,
                                                                                    is home to approximately 3 million
                                                                                    people and 300 waria, who usually
                                                                                    live in isolated communities. They
                                                                                    face a certain level of
                                                                                    marginalization and discrimination,
                                                                                    especially
                                                                                    when they encounter pockets of
                                                                                    Islamic radicalism. But surprisingly
                                                                                    warias are also quite accepted in
                                                                                    Indonesian society. You can find
                                                                                    warias everywhere: in salons, on
                                                                                    television, at weddings, on street
                                                                                    corners.<br /> Despite this
                                                                                    situation they are prone to
                                                                                    ridicule, violence, and poverty.
                                                                                    Their job opportunities are
                                                                                    generally limited
                                                                                    to street performing, prostitution,
                                                                                    working in beauty salons, or acting
                                                                                    on television, playing caricatures
                                                                                    of themselves.<br /> Many warias
                                                                                    have also no families and no
                                                                                    legal identities. Some of them leave
                                                                                    the island where they were born to
                                                                                    go to Yogyakarta where the
                                                                                    atmosphere is more tolerant and
                                                                                    relaxed. Their families don’t know
                                                                                    who
                                                                                    they really are. They play waria at
                                                                                    night and man during the day. Others
                                                                                    can count on their own families and
                                                                                    run a fairly normal life as
                                                                                    women.<br /> Even though  waria
                                                                                    assume the identity of women, they
                                                                                    usually retain their male
                                                                                    reproductive organs, because
                                                                                    operations are far too expensive.
                                                                                    The few who can afford an operation
                                                                                    have their
                                                                                    breasts done, all the others face an
                                                                                    horrible and unhealthy practice
                                                                                    where the silicone is forced
                                                                                    straight under the skin. Since many
                                                                                    are prostitutes they also face HIV
                                                                                    and sexual diseases.<br /> Only some
                                                                                    of them have stable relationships
                                                                                    with a man.<br /> Even though in
                                                                                    Indonesia waria are quite tollerate,
                                                                                    acceptance of transgender
                                                                                    Muslims is still not widespread. For
                                                                                    this reason Shinta Ratri a LGBT
                                                                                    activist opened in her house a
                                                                                    Pesantren Waria (Islamic boarding
                                                                                    school for transgenders), the first
                                                                                    and only one of its kind in
                                                                                    Indonesia and possibly in the world.
                                                                                    Although it is called a boarding
                                                                                    school, the Pondok Pesantren Waria
                                                                                    Al-Fatah functions more like a
                                                                                    religious
                                                                                    school where students can learn
                                                                                    classical religious subjects like
                                                                                    fiqh and Qur’an recitation, as well
                                                                                    as a community centre where students
                                                                                    gather to break the fast together
                                                                                    during Ramadan.<br /> This is her
                                                                                    story.</p>
                                                                            </div> -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <script id="script-114935" data-row="script-114935" type="text/javascript" class="vc_controls">
                                                            UNCODE.initRow(document.getElementById("script-114935"));
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                </article>
            </div>
            <!-- sections container -->
        </div>
        <!-- page wrapper -->
        <footer id="colophon" class="site-footer">
            <div class="footer-content-block row-container  style-light-bg style-light">
                <div class="footer-content-block-inner limit-width row-parent"></div>
                <!-- /.footer-content-block -->
            </div>
            <!-- /.footer-content-block-inner -->
            <div class="row-container style-dark-bg footer-last desktop-hidden">
                <div class="row row-parent style-dark limit-width no-top-padding no-h-padding no-bottom-padding">
                    <div class="site-info uncell col-lg-6 pos-middle text-left">
                        <p>©Copyright 2020 – Fulvio Bugani<br /><a href="http://www.fulviobugani.com/privacy-policy/">Privacy Policy</a> | <a href="http://www.fulviobugani.com/cookie-policy/">Cookie Policy</a></p>
                    </div>
                    <!-- site info -->
                    <div class="uncell col-lg-6 pos-middle text-right">
                        <div class="social-icon icon-box icon-box-top icon-inline"><a href="https://www.facebook.com/FulvioBuganiFotoimage/" target="_blank"><i class="fa fa-facebook-square"></i></a></div>
                        <div class="social-icon icon-box icon-box-top icon-inline"><a href="https://www.instagram.com/fulviobugani_fotoimage/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- main container -->
</div>
<!-- main wrapper -->
</div>
<!-- box container -->
</div>
<!-- box wrapper -->
<div class="style-light footer-scroll-top"><a href="#" class="scroll-top"><i class="fa fa-angle-up fa-stack fa-rounded btn-default btn-hover-nobg"></i></a></div>
<!--googleoff: all-->
<div id="cookie-law-info-bar" data-nosnippet="true"><span>
        <div class="cli-bar-container cli-style-v2">
            <div class="cli-bar-message">Questo sito fa uso di cookie per migliorare l’esperienza di navigazione
                degli utenti e per raccogliere informazioni sull’utilizzo del sito stesso. Proseguendo nella
                navigazione si accetta l’uso dei cookie; in caso contrario è possibile abbandonare il sito.</div>
            <div class="cli-bar-btn_container"><a href='https://www.fulviobugani.com/cookie-policy/' id="CONSTANT_OPEN_URL" target="_blank" class="cli-plugin-main-link" style="display:inline-block; margin:0px 10px 0px 5px; ">Cookie Policy</a><a role='button' tabindex='0' data-cli_action="accept" id="cookie_action_close_header" class="medium cli-plugin-button cli-plugin-main-button cookie_action_close_header cli_action_button" style="display:inline-block; ">Accetto</a></div>
        </div>
    </span></div>
<div id="cookie-law-info-again" style="display:none;" data-nosnippet="true"><span id="cookie_hdr_showagain">Manage
        consent</span></div>
<div class="cli-modal" data-nosnippet="true" id="cliSettingsPopup" tabindex="-1" role="dialog" aria-labelledby="cliSettingsPopup" aria-hidden="true">
    <div class="cli-modal-dialog" role="document">
        <div class="cli-modal-content cli-bar-popup">
            <button type="button" class="cli-modal-close" id="cliModalClose">
                <svg class="" viewBox="0 0 24 24">
                    <path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z">
                    </path>
                    <path d="M0 0h24v24h-24z" fill="none"></path>
                </svg>
                <span class="wt-cli-sr-only">Close</span>
            </button>
            <div class="cli-modal-body">
                <div class="cli-container-fluid cli-tab-container">
                    <div class="cli-row">
                        <div class="cli-col-12 cli-align-items-stretch cli-px-0">
                            <div class="cli-privacy-overview">
                                <h4>Privacy Overview</h4>
                                <div class="cli-privacy-content">
                                    <div class="cli-privacy-content-text">This website uses cookies to improve your
                                        experience while you navigate through the website. Out of these, the cookies
                                        that are categorized as necessary are stored on your browser as they are
                                        essential for the
                                        working of basic functionalities of the website. We also use third-party
                                        cookies that help us analyze and understand how you use this website. These
                                        cookies will be stored in your browser only with your
                                        consent. You also have the option to opt-out of these cookies. But opting
                                        out of some of these cookies may affect your browsing experience.</div>
                                </div>
                                <a class="cli-privacy-readmore" data-readmore-text="Show more" data-readless-text="Show less"></a>
                            </div>
                        </div>
                        <div class="cli-col-12 cli-align-items-stretch cli-px-0 cli-tab-section-container">

                            <div class="cli-tab-section">
                                <div class="cli-tab-header">
                                    <a role="button" tabindex="0" class="cli-nav-link cli-settings-mobile" data-target="necessary" data-toggle="cli-toggle-tab">
                                        Necessary </a>
                                    <div class="wt-cli-necessary-checkbox">
                                        <input type="checkbox" class="cli-user-preference-checkbox" id="wt-cli-checkbox-necessary" data-id="checkbox-necessary" checked="checked" />
                                        <label class="form-check-label" for="wt-cli-checkbox-necessary">Necessary</label>
                                    </div>
                                    <span class="cli-necessary-caption">Always Enabled</span>
                                </div>
                                <div class="cli-tab-content">
                                    <div class="cli-tab-pane cli-fade" data-id="necessary">
                                        <p>Necessary cookies are absolutely essential for the website to function
                                            properly. This category only includes cookies that ensures basic
                                            functionalities and security features of the website. These cookies
                                            do not store any personal information.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-tab-section">
                                <div class="cli-tab-header">
                                    <a role="button" tabindex="0" class="cli-nav-link cli-settings-mobile" data-target="non-necessary" data-toggle="cli-toggle-tab">
                                        Non-necessary </a>
                                    <div class="cli-switch">
                                        <input type="checkbox" id="wt-cli-checkbox-non-necessary" class="cli-user-preference-checkbox" data-id="checkbox-non-necessary" checked='checked' />
                                        <label for="wt-cli-checkbox-non-necessary" class="cli-slider" data-cli-enable="Enabled" data-cli-disable="Disabled"><span class="wt-cli-sr-only">Non-necessary</span></label>
                                    </div>
                                </div>
                                <div class="cli-tab-content">
                                    <div class="cli-tab-pane cli-fade" data-id="non-necessary">
                                        <p>Any cookies that may not be particularly necessary for the website to
                                            function and is used specifically to collect user personal data via
                                            analytics, ads, other embedded contents are termed as non-necessary
                                            cookies. It is mandatory to procure user consent prior to running these
                                            cookies on your website.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
<div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
<!--googleon: all-->
<div class="gdpr-overlay"></div>
<div class="gdpr gdpr-privacy-preferences">
    <div class="gdpr-wrapper">
        <form method="post" class="gdpr-privacy-preferences-frm" action="#">
            <input type="hidden" name="action" value="uncode_privacy_update_privacy_preferences">
            <input type="hidden" id="update-privacy-preferences-nonce" name="update-privacy-preferences-nonce" value="4818ca3d2f" /><input type="hidden" name="_wp_http_referer" value="/waria/" />
            <header>
                <div class="gdpr-box-title">
                    <h3>Privacy Preference Center</h3>
                    <span class="gdpr-close"></span>
                </div>
            </header>
            <div class="gdpr-content">
                <div class="gdpr-tab-content">
                    <div class="gdpr-consent-management gdpr-active">
                        <header>
                            <h4>Privacy Preferences</h4>
                        </header>
                        <div class="gdpr-info">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
            <footer>
                <input type="submit" class="btn-accent btn-flat" value="Save Preferences">
            </footer>
        </form>
    </div>
</div>
<script type="text/javascript">
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");

        var x = document.getElementById("myDropdown2");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }

    }

    function myFunction2() {
        document.getElementById("hiddentext").classList.toggle("show");


    }
</script>
<script type="text/html" id="wpb-modifications"></script>
<script type="text/javascript" src="http://blog.test/wp-includes/js/underscore.min.js?ver=1.8.3"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var DavesWordPressLiveSearchConfig = {
        "resultsDirection": "",
        "showThumbs": "false",
        "showExcerpt": "false",
        "displayPostCategory": "false",
        "showMoreResultsLink": "true",
        "activateWidgetLink": "true",
        "minCharsToSearch": "0",
        "xOffset": "0",
        "yOffset": "0",
        "blogURL": "https:\/\/www.fulviobugani.com",
        "ajaxURL": "https:\/\/www.fulviobugani.com\/wp-admin\/admin-ajax.php",
        "viewMoreText": "View more results",
        "outdatedJQuery": "Dave's WordPress Live Search requires jQuery 1.2.6 or higher. WordPress ships with current jQuery versions. But if you are seeing this message, it's likely that another plugin is including an earlier version.",
        "resultTemplate": "<ul id=\"dwls_search_results\" class=\"search_results dwls_search_results\" role=\"presentation\" aria-hidden=\"true\">\n<input type=\"hidden\" name=\"query\" value=\"<%- resultsSearchTerm %>\" \/>\n<% _.each(searchResults, function(searchResult, index, list) { %>\n        <%\n        \/\/ Thumbnails\n        if(DavesWordPressLiveSearchConfig.showThumbs == \"true\" && searchResult.attachment_thumbnail) {\n                liClass = \"post_with_thumb\";\n        }\n        else {\n                liClass = \"\";\n        }\n        %>\n        <li class=\"post-<%= searchResult.ID %> daves-wordpress-live-search_result <%- liClass %>\">\n\n        <a href=\"<%= searchResult.permalink %>\" class=\"daves-wordpress-live-search_title\">\n        <% if(DavesWordPressLiveSearchConfig.displayPostCategory == \"true\" && searchResult.post_category !== undefined) { %>\n                <span class=\"search-category\"><%= searchResult.post_category %><\/span>\n        <% } %><span class=\"search-title\"><%= searchResult.post_title %><\/span><\/a>\n\n        <% if(searchResult.post_price !== undefined) { %>\n                <p class=\"price\"><%- searchResult.post_price %><\/p>\n        <% } %>\n\n        <% if(DavesWordPressLiveSearchConfig.showExcerpt == \"true\" && searchResult.post_excerpt) { %>\n                <%= searchResult.post_excerpt %>\n        <% } %>\n\n        <% if(e.displayPostMeta) { %>\n                <p class=\"meta clearfix daves-wordpress-live-search_author\" id=\"daves-wordpress-live-search_author\">Posted by <%- searchResult.post_author_nicename %><\/p><p id=\"daves-wordpress-live-search_date\" class=\"meta clearfix daves-wordpress-live-search_date\"><%- searchResult.post_date %><\/p>\n        <% } %>\n        <div class=\"clearfix\"><\/div><\/li>\n<% }); %>\n\n<% if(searchResults[0].show_more !== undefined && searchResults[0].show_more && DavesWordPressLiveSearchConfig.showMoreResultsLink == \"true\") { %>\n        <div class=\"clearfix search_footer\"><a href=\"<%= DavesWordPressLiveSearchConfig.blogURL %>\/?s=<%-  resultsSearchTerm %>\"><%- DavesWordPressLiveSearchConfig.viewMoreText %><\/a><\/div>\n<% } %>\n\n<\/ul>\n"
    };
    /* ]]> */
</script>
<script type="text/javascript" src="http://blog.test/wp-content/plugins/uncode-daves-wordpress-live-search/js/daves-wordpress-live-search.js?ver=5.4.15"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/www.fulviobugani.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    /* ]]> */
</script>
<script type="text/javascript" src="http://blog.test/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.9"></script>
<script type="text/javascript" src="http://blog.test/wp-content/plugins/uncode-privacy/assets/js/js-cookie.min.js?ver=2.2.0"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var Uncode_Privacy_Parameters = {
        "accent_color": "#0d669d"
    };
    /* ]]> */
</script>
<script type="text/javascript" src="http://blog.test/wp-content/plugins/uncode-privacy/assets/js/uncode-privacy-public.min.js?ver=2.1.1"></script>
<script type='text/javascript'>
    var mejsL10n = {
        "language": "en",
        "strings": {
            "mejs.download-file": "Download File",
            "mejs.install-flash": "You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/",
            "mejs.fullscreen": "Fullscreen",
            "mejs.play": "Play",
            "mejs.pause": "Pause",
            "mejs.time-slider": "Time Slider",
            "mejs.time-help-text": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.",
            "mejs.live-broadcast": "Live Broadcast",
            "mejs.volume-help-text": "Use Up\/Down Arrow keys to increase or decrease volume.",
            "mejs.unmute": "Unmute",
            "mejs.mute": "Mute",
            "mejs.volume-slider": "Volume Slider",
            "mejs.video-player": "Video Player",
            "mejs.audio-player": "Audio Player",
            "mejs.captions-subtitles": "Captions\/Subtitles",
            "mejs.captions-chapters": "Chapters",
            "mejs.none": "None",
            "mejs.afrikaans": "Afrikaans",
            "mejs.albanian": "Albanian",
            "mejs.arabic": "Arabic",
            "mejs.belarusian": "Belarusian",
            "mejs.bulgarian": "Bulgarian",
            "mejs.catalan": "Catalan",
            "mejs.chinese": "Chinese",
            "mejs.chinese-simplified": "Chinese (Simplified)",
            "mejs.chinese-traditional": "Chinese (Traditional)",
            "mejs.croatian": "Croatian",
            "mejs.czech": "Czech",
            "mejs.danish": "Danish",
            "mejs.dutch": "Dutch",
            "mejs.english": "English",
            "mejs.estonian": "Estonian",
            "mejs.filipino": "Filipino",
            "mejs.finnish": "Finnish",
            "mejs.french": "French",
            "mejs.galician": "Galician",
            "mejs.german": "German",
            "mejs.greek": "Greek",
            "mejs.haitian-creole": "Haitian Creole",
            "mejs.hebrew": "Hebrew",
            "mejs.hindi": "Hindi",
            "mejs.hungarian": "Hungarian",
            "mejs.icelandic": "Icelandic",
            "mejs.indonesian": "Indonesian",
            "mejs.irish": "Irish",
            "mejs.italian": "Italian",
            "mejs.japanese": "Japanese",
            "mejs.korean": "Korean",
            "mejs.latvian": "Latvian",
            "mejs.lithuanian": "Lithuanian",
            "mejs.macedonian": "Macedonian",
            "mejs.malay": "Malay",
            "mejs.maltese": "Maltese",
            "mejs.norwegian": "Norwegian",
            "mejs.persian": "Persian",
            "mejs.polish": "Polish",
            "mejs.portuguese": "Portuguese",
            "mejs.romanian": "Romanian",
            "mejs.russian": "Russian",
            "mejs.serbian": "Serbian",
            "mejs.slovak": "Slovak",
            "mejs.slovenian": "Slovenian",
            "mejs.spanish": "Spanish",
            "mejs.swahili": "Swahili",
            "mejs.swedish": "Swedish",
            "mejs.tagalog": "Tagalog",
            "mejs.thai": "Thai",
            "mejs.turkish": "Turkish",
            "mejs.ukrainian": "Ukrainian",
            "mejs.vietnamese": "Vietnamese",
            "mejs.welsh": "Welsh",
            "mejs.yiddish": "Yiddish"
        }
    };
</script>
<script type="text/javascript" src="http://blog.test/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.13-9993131"></script>
<script type="text/javascript" src="http://blog.test/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.4.15"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpmejsSettings = {
        "pluginPath": "\/wp-includes\/js\/mediaelement\/",
        "classPrefix": "mejs-",
        "stretching": "responsive"
    };
    /* ]]> */
</script>
<script type="text/javascript" src="http://blog.test/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.4.15"></script>
<script type="text/javascript" src="http://blog.test/wp-content/themes/blog/uncode/library/js/plugins.js?ver=142619648"></script>
<script type="text/javascript" src="http://blog.test/wp-content/themes/blog/uncode/library/js/app.js?ver=142619648"></script>
<script type="text/javascript" src="http://blog.test/wp-includes/js/wp-embed.min.js?ver=5.4.15"></script>
</main>

<?php get_footer(); ?>