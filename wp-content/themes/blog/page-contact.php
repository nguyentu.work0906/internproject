<?php
/*
Template Name: Contact Page
*/
get_header();
?>

<div class="main-wrapper">
    <div class="main-container">
        <div class="page-wrapper">
            <div class="sections-container">
                <script type="text/javascript">
                    UNCODE.initHeader();
                </script>
                <article id="post-15" class="page-body style-light-bg post-15 page type-page status-publish hentry">
                    <div class="post-wrapper">
                        <div class="post-body">
                            <div class="post-content un-no-sidebar-layout">
                                <div class="row-container">
                                    <div class="row row-parent style-light limit-width double-top-padding double-bottom-padding">
                                        <div class="post-title-wrapper">
                                            <h1 class="post-title">Contact</h1>
                                        </div>
                                        <p><img class="wp-image-309 size-large aligncenter" src="http://www.fulviobugani.com/wp-content/uploads/2015/12/cooking-2-400x600.png" alt="fulvio bugani contact details" width="400" height="600" srcset="https://www.fulviobugani.com/wp-content/uploads/2015/12/cooking-2-400x600.png 400w, https://www.fulviobugani.com/wp-content/uploads/2015/12/cooking-2-133x200.png 133w, https://www.fulviobugani.com/wp-content/uploads/2015/12/cooking-2-267x400.png 267w, https://www.fulviobugani.com/wp-content/uploads/2015/12/cooking-2.png 735w" sizes="(max-width: 400px) 100vw, 400px" /></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div><!-- sections container -->
        </div><!-- page wrapper -->
        <!-- <footer id="colophon" class="site-footer">
            <div class="footer-content-block row-container  style-light-bg style-light">
                <div class="footer-content-block-inner limit-width row-parent"></div>
            </div>
            <div class="row-container style-dark-bg footer-last desktop-hidden">
                <div class="row row-parent style-dark limit-width no-top-padding no-h-padding no-bottom-padding">
                    <div class="site-info uncell col-lg-6 pos-middle text-left">
                        <p>©Copyright 2020 – Fulvio Bugani<br /><a href="http://www.fulviobugani.com/privacy-policy/">Privacy Policy</a> | <a href="http://www.fulviobugani.com/cookie-policy/">Cookie Policy</a></p>
                    </div>
                    <div class="uncell col-lg-6 pos-middle text-right">
                        <div class="social-icon icon-box icon-box-top icon-inline"><a href="https://www.facebook.com/FulvioBuganiFotoimage/" target="_blank"><i class="fa fa-facebook-square"></i></a></div>
                        <div class="social-icon icon-box icon-box-top icon-inline"><a href="https://www.instagram.com/fulviobugani_fotoimage/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></div>
                    </div>
                </div>
            </div>
        </footer> -->
    </div><!-- main container -->
</div><!-- main wrapper -->
</div><!-- box container -->
</div><!-- box wrapper -->
<div class="style-light footer-scroll-top"><a href="#" class="scroll-top"><i class="fa fa-angle-up fa-stack fa-rounded btn-default btn-hover-nobg"></i></a></div><!--googleoff: all-->
<div id="cookie-law-info-bar" data-nosnippet="true"><span>
        <div class="cli-bar-container cli-style-v2">
            <div class="cli-bar-message">Questo sito fa uso di cookie per migliorare l’esperienza di navigazione degli utenti e per raccogliere informazioni sull’utilizzo del sito stesso. Proseguendo nella navigazione si accetta l’uso dei cookie; in caso contrario è possibile abbandonare il sito.</div>
            <div class="cli-bar-btn_container"><a href='http://blog.test/cookie-policy/' id="CONSTANT_OPEN_URL" target="_blank" class="cli-plugin-main-link" style="display:inline-block; margin:0px 10px 0px 5px; ">Cookie Policy</a><a role='button' tabindex='0' data-cli_action="accept" id="cookie_action_close_header" class="medium cli-plugin-button cli-plugin-main-button cookie_action_close_header cli_action_button" style="display:inline-block; ">Accetto</a></div>
        </div>
    </span></div>
<div id="cookie-law-info-again" style="display:none;" data-nosnippet="true"><span id="cookie_hdr_showagain">Manage consent</span></div>
<div class="cli-modal" data-nosnippet="true" id="cliSettingsPopup" tabindex="-1" role="dialog" aria-labelledby="cliSettingsPopup" aria-hidden="true">
    <div class="cli-modal-dialog" role="document">
        <div class="cli-modal-content cli-bar-popup">
            <button type="button" class="cli-modal-close" id="cliModalClose">
                <svg class="" viewBox="0 0 24 24">
                    <path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path>
                    <path d="M0 0h24v24h-24z" fill="none"></path>
                </svg>
                <span class="wt-cli-sr-only">Close</span>
            </button>
            <div class="cli-modal-body">
                <div class="cli-container-fluid cli-tab-container">
                    <div class="cli-row">
                        <div class="cli-col-12 cli-align-items-stretch cli-px-0">
                            <div class="cli-privacy-overview">
                                <h4>Privacy Overview</h4>
                                <div class="cli-privacy-content">
                                    <div class="cli-privacy-content-text">This website uses cookies to improve your experience while you navigate through the website. Out of these, the cookies that are categorized as necessary are stored on your browser as they are essential for the working of basic functionalities of the website. We also use third-party cookies that help us analyze and understand how you use this website. These cookies will be stored in your browser only with your consent. You also have the option to opt-out of these cookies. But opting out of some of these cookies may affect your browsing experience.</div>
                                </div>
                                <a class="cli-privacy-readmore" data-readmore-text="Show more" data-readless-text="Show less"></a>
                            </div>
                        </div>
                        <div class="cli-col-12 cli-align-items-stretch cli-px-0 cli-tab-section-container">

                            <div class="cli-tab-section">
                                <div class="cli-tab-header">
                                    <a role="button" tabindex="0" class="cli-nav-link cli-settings-mobile" data-target="necessary" data-toggle="cli-toggle-tab">
                                        Necessary </a>
                                    <div class="wt-cli-necessary-checkbox">
                                        <input type="checkbox" class="cli-user-preference-checkbox" id="wt-cli-checkbox-necessary" data-id="checkbox-necessary" checked="checked" />
                                        <label class="form-check-label" for="wt-cli-checkbox-necessary">Necessary</label>
                                    </div>
                                    <span class="cli-necessary-caption">Always Enabled</span>
                                </div>
                                <div class="cli-tab-content">
                                    <div class="cli-tab-pane cli-fade" data-id="necessary">
                                        <p>Necessary cookies are absolutely essential for the website to function properly. This category only includes cookies that ensures basic functionalities and security features of the website. These cookies do not store any personal information.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-tab-section">
                                <div class="cli-tab-header">
                                    <a role="button" tabindex="0" class="cli-nav-link cli-settings-mobile" data-target="non-necessary" data-toggle="cli-toggle-tab">
                                        Non-necessary </a>
                                    <div class="cli-switch">
                                        <input type="checkbox" id="wt-cli-checkbox-non-necessary" class="cli-user-preference-checkbox" data-id="checkbox-non-necessary" checked='checked' />
                                        <label for="wt-cli-checkbox-non-necessary" class="cli-slider" data-cli-enable="Enabled" data-cli-disable="Disabled"><span class="wt-cli-sr-only">Non-necessary</span></label>
                                    </div>
                                </div>
                                <div class="cli-tab-content">
                                    <div class="cli-tab-pane cli-fade" data-id="non-necessary">
                                        <p>Any cookies that may not be particularly necessary for the website to function and is used specifically to collect user personal data via analytics, ads, other embedded contents are termed as non-necessary cookies. It is mandatory to procure user consent prior to running these cookies on your website.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
<div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
<!--googleon: all-->
<div class="gdpr-overlay"></div>
<div class="gdpr gdpr-privacy-preferences">
    <div class="gdpr-wrapper">
        <form method="post" class="gdpr-privacy-preferences-frm" action="https://www.fulviobugani.com/wp-admin/admin-post.php">
            <input type="hidden" name="action" value="uncode_privacy_update_privacy_preferences">
            <input type="hidden" id="update-privacy-preferences-nonce" name="update-privacy-preferences-nonce" value="6360e636fc" /><input type="hidden" name="_wp_http_referer" value="/about-me/" />
            <header>
                <div class="gdpr-box-title">
                    <h3>Privacy Preference Center</h3>
                    <span class="gdpr-close"></span>
                </div>
            </header>
            <div class="gdpr-content">
                <div class="gdpr-tab-content">
                    <div class="gdpr-consent-management gdpr-active">
                        <header>
                            <h4>Privacy Preferences</h4>
                        </header>
                        <div class="gdpr-info">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
            <footer>
                <input type="submit" class="btn-accent btn-flat" value="Save Preferences">
            </footer>
        </form>
    </div>
</div>
<script type="text/javascript">
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");

        var x = document.getElementById("myDropdown2");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }

    }

    function myFunction2() {
        document.getElementById("hiddentext").classList.toggle("show");


    }
</script>
<script type="text/html" id="wpb-modifications"></script>
<script type='text/javascript' src='http://blog.test/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var DavesWordPressLiveSearchConfig = {
        "resultsDirection": "",
        "showThumbs": "false",
        "showExcerpt": "false",
        "displayPostCategory": "false",
        "showMoreResultsLink": "true",
        "activateWidgetLink": "true",
        "minCharsToSearch": "0",
        "xOffset": "0",
        "yOffset": "0",
        "blogURL": "https:\/\/www.fulviobugani.com",
        "ajaxURL": "https:\/\/www.fulviobugani.com\/wp-admin\/admin-ajax.php",
        "viewMoreText": "View more results",
        "outdatedJQuery": "Dave's WordPress Live Search requires jQuery 1.2.6 or higher. WordPress ships with current jQuery versions. But if you are seeing this message, it's likely that another plugin is including an earlier version.",
        "resultTemplate": "<ul id=\"dwls_search_results\" class=\"search_results dwls_search_results\" role=\"presentation\" aria-hidden=\"true\">\n<input type=\"hidden\" name=\"query\" value=\"<%- resultsSearchTerm %>\" \/>\n<% _.each(searchResults, function(searchResult, index, list) { %>\n        <%\n        \/\/ Thumbnails\n        if(DavesWordPressLiveSearchConfig.showThumbs == \"true\" && searchResult.attachment_thumbnail) {\n                liClass = \"post_with_thumb\";\n        }\n        else {\n                liClass = \"\";\n        }\n        %>\n        <li class=\"post-<%= searchResult.ID %> daves-wordpress-live-search_result <%- liClass %>\">\n\n        <a href=\"<%= searchResult.permalink %>\" class=\"daves-wordpress-live-search_title\">\n        <% if(DavesWordPressLiveSearchConfig.displayPostCategory == \"true\" && searchResult.post_category !== undefined) { %>\n                <span class=\"search-category\"><%= searchResult.post_category %><\/span>\n        <% } %><span class=\"search-title\"><%= searchResult.post_title %><\/span><\/a>\n\n        <% if(searchResult.post_price !== undefined) { %>\n                <p class=\"price\"><%- searchResult.post_price %><\/p>\n        <% } %>\n\n        <% if(DavesWordPressLiveSearchConfig.showExcerpt == \"true\" && searchResult.post_excerpt) { %>\n                <%= searchResult.post_excerpt %>\n        <% } %>\n\n        <% if(e.displayPostMeta) { %>\n                <p class=\"meta clearfix daves-wordpress-live-search_author\" id=\"daves-wordpress-live-search_author\">Posted by <%- searchResult.post_author_nicename %><\/p><p id=\"daves-wordpress-live-search_date\" class=\"meta clearfix daves-wordpress-live-search_date\"><%- searchResult.post_date %><\/p>\n        <% } %>\n        <div class=\"clearfix\"><\/div><\/li>\n<% }); %>\n\n<% if(searchResults[0].show_more !== undefined && searchResults[0].show_more && DavesWordPressLiveSearchConfig.showMoreResultsLink == \"true\") { %>\n        <div class=\"clearfix search_footer\"><a href=\"<%= DavesWordPressLiveSearchConfig.blogURL %>\/?s=<%-  resultsSearchTerm %>\"><%- DavesWordPressLiveSearchConfig.viewMoreText %><\/a><\/div>\n<% } %>\n\n<\/ul>\n"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='http://blog.test/wp-content/plugins/uncode-daves-wordpress-live-search/js/daves-wordpress-live-search.js?ver=5.4.15'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/www.fulviobugani.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    /* ]]> */
</script>
<script type='text/javascript' src='http://blog.test/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.9'></script>
<script type='text/javascript' src='http://blog.test/wp-content/plugins/uncode-privacy/assets/js/js-cookie.min.js?ver=2.2.0'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var Uncode_Privacy_Parameters = {
        "accent_color": "#0d669d"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='http://blog.test/wp-content/plugins/uncode-privacy/assets/js/uncode-privacy-public.min.js?ver=2.1.1'></script>
<script type='text/javascript'>
    var mejsL10n = {
        "language": "en",
        "strings": {
            "mejs.download-file": "Download File",
            "mejs.install-flash": "You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/",
            "mejs.fullscreen": "Fullscreen",
            "mejs.play": "Play",
            "mejs.pause": "Pause",
            "mejs.time-slider": "Time Slider",
            "mejs.time-help-text": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.",
            "mejs.live-broadcast": "Live Broadcast",
            "mejs.volume-help-text": "Use Up\/Down Arrow keys to increase or decrease volume.",
            "mejs.unmute": "Unmute",
            "mejs.mute": "Mute",
            "mejs.volume-slider": "Volume Slider",
            "mejs.video-player": "Video Player",
            "mejs.audio-player": "Audio Player",
            "mejs.captions-subtitles": "Captions\/Subtitles",
            "mejs.captions-chapters": "Chapters",
            "mejs.none": "None",
            "mejs.afrikaans": "Afrikaans",
            "mejs.albanian": "Albanian",
            "mejs.arabic": "Arabic",
            "mejs.belarusian": "Belarusian",
            "mejs.bulgarian": "Bulgarian",
            "mejs.catalan": "Catalan",
            "mejs.chinese": "Chinese",
            "mejs.chinese-simplified": "Chinese (Simplified)",
            "mejs.chinese-traditional": "Chinese (Traditional)",
            "mejs.croatian": "Croatian",
            "mejs.czech": "Czech",
            "mejs.danish": "Danish",
            "mejs.dutch": "Dutch",
            "mejs.english": "English",
            "mejs.estonian": "Estonian",
            "mejs.filipino": "Filipino",
            "mejs.finnish": "Finnish",
            "mejs.french": "French",
            "mejs.galician": "Galician",
            "mejs.german": "German",
            "mejs.greek": "Greek",
            "mejs.haitian-creole": "Haitian Creole",
            "mejs.hebrew": "Hebrew",
            "mejs.hindi": "Hindi",
            "mejs.hungarian": "Hungarian",
            "mejs.icelandic": "Icelandic",
            "mejs.indonesian": "Indonesian",
            "mejs.irish": "Irish",
            "mejs.italian": "Italian",
            "mejs.japanese": "Japanese",
            "mejs.korean": "Korean",
            "mejs.latvian": "Latvian",
            "mejs.lithuanian": "Lithuanian",
            "mejs.macedonian": "Macedonian",
            "mejs.malay": "Malay",
            "mejs.maltese": "Maltese",
            "mejs.norwegian": "Norwegian",
            "mejs.persian": "Persian",
            "mejs.polish": "Polish",
            "mejs.portuguese": "Portuguese",
            "mejs.romanian": "Romanian",
            "mejs.russian": "Russian",
            "mejs.serbian": "Serbian",
            "mejs.slovak": "Slovak",
            "mejs.slovenian": "Slovenian",
            "mejs.spanish": "Spanish",
            "mejs.swahili": "Swahili",
            "mejs.swedish": "Swedish",
            "mejs.tagalog": "Tagalog",
            "mejs.thai": "Thai",
            "mejs.turkish": "Turkish",
            "mejs.ukrainian": "Ukrainian",
            "mejs.vietnamese": "Vietnamese",
            "mejs.welsh": "Welsh",
            "mejs.yiddish": "Yiddish"
        }
    };
</script>
<script type='text/javascript' src='http://blog.test/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.13-9993131'></script>
<script type='text/javascript' src='http://blog.test/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.4.15'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpmejsSettings = {
        "pluginPath": "\/wp-includes\/js\/mediaelement\/",
        "classPrefix": "mejs-",
        "stretching": "responsive"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='http://blog.test/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.4.15'></script>
<script type='text/javascript' src='http://blog.test/wp-content/themes/blog/uncode/library/js/plugins.js?ver=1439317685'></script>
<script type='text/javascript' src='http://blog.test/wp-content/themes/blog/uncode/library/js/app.js?ver=1439317685'></script>
<script type='text/javascript' src='http://blog.test/wp-includes/js/wp-embed.min.js?ver=5.4.15'></script>


<?php get_footer(); ?>