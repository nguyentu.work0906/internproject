<?php

function blog_theme_support()
{
    add_theme_support('template');
    add_theme_support('title-tag');
    add_theme_support('custom-logo', array(
        'height'      => 100, // Chiều cao logo tối đa
        'width'       => 400, // Chiều rộng logo tối đa
        'flex-height' => true,
        'flex-width'  => true,
    ));
}
add_action("after_setup_theme", "blog_theme_support");


function blog_register_styles()
{
    $version = wp_get_theme()->get('Version');

    wp_enqueue_style('blog-style', get_template_directory_uri() . "/style.css", array('blog-bootstrap'), $version, 'all');
    wp_enqueue_style('blog-bootstrap', "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css", array(), '4.4.1', 'all');
    wp_enqueue_style('blog-fontawesome', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css", array(), '5.13.0', 'all');
}
add_action("wp_enqueue_scripts", "blog_register_styles");





function blog_register_scripts()
{
    $version = wp_get_theme()->get('version');

    wp_enqueue_script('blog-jquery', "https://code.jquery.com/jquery-3.4.1.slim.min.js", array(), '3.4.1', true);
    wp_enqueue_script('blog-popper', "https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js", array(), '1.16.0', true);
    wp_enqueue_style('blog-bootstrap', "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js", array(), '3.4.1', true);
    wp_enqueue_script('blog-main', get_template_directory_uri() . "/assets/js.main.js", array(), '1.0', true);
}
add_action("wp_enqueue_scripts", "blog_register_scripts");


function register_my_menus()
{
    register_nav_menus(
        array(
            'header-menu' => __('Header Menu'),
            'footer-menu' => __('Footer Menu')
        )
    );
}
add_action('init', 'register_my_menus');



function add_custom_css()
{
    wp_enqueue_style('layerslider-style', get_template_directory_uri() . '/layerslider.css', array(), '1.0', 'all');
}
add_action('wp_enqueue_scripts', 'add_custom_css');

function load_font_awesome()
{
    wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css');
}
add_action('wp_enqueue_scripts', 'load_font_awesome');
