<!DOCTYPE html>
<html class="no-touch" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="https://www.fulviobugani.com/xmlrpc.php">
    <title><?php wp_title('&#8211;', true, 'right');
            echo 'Quang Nguyen &#8211; Photographer'; ?></title>
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Fulvio Bugani &raquo; Feed" href="https://www.fulviobugani.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Fulvio Bugani &raquo; Comments Feed" href="https://www.fulviobugani.com/comments/feed/" />
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {
                "concatemoji": "https:\/\/www.fulviobugani.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.15"
            }
        };
        /*! This file is auto-generated */
        ! function(e, a, t) {
            var n, r, o, i = a.createElement("canvas"),
                p = i.getContext && i.getContext("2d");

            function s(e, t) {
                var a = String.fromCharCode;
                p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0);
                e = i.toDataURL();
                return p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL()
            }

            function c(e) {
                var t = a.createElement("script");
                t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
            }
            for (o = Array("flag", "emoji"), t.supports = {
                    everything: !0,
                    everythingExceptFlag: !0
                }, r = 0; r < o.length; r++) t.supports[o[r]] = function(e) {
                if (!p || !p.fillText) return !1;
                switch (p.textBaseline = "top", p.font = "600 32px Arial", e) {
                    case "flag":
                        return s([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) ? !1 : !s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !s([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]);
                    case "emoji":
                        return !s([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340])
                }
                return !1
            }(o[r]), t.supports.everything = t.supports.everything && t.supports[o[r]], "flag" !== o[r] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]);
            t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function() {
                t.DOMReady = !0
            }, t.supports.everything || (n = function() {
                t.readyCallback()
            }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() {
                "complete" === a.readyState && t.readyCallback()
            })), (n = t.source || {}).concatemoji ? c(n.concatemoji) : n.wpemoji && n.twemoji && (c(n.twemoji), c(n.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" id="layerslider-css" href="http://blog.test/wp-content/plugins/LayerSlider/static/layerslider/css/layerslider.css" type="text/css" media="all" />
    <link rel="stylesheet" id="wp-block-library-css" href="http://blog.test/wp-includes/css/dist/block-library/style.min.css?ver=5.4.15" type="text/css" media="all" />
    <link rel="stylesheet" id="contact-form-7-css" href="http://blog.test/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.9" type="text/css" media="all" />
    <link rel="stylesheet" id="cookie-law-info-css" href="http://blog.test/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-public.css?ver=1.9.4" type="text/css" media="all" />
    <link rel='stylesheet' id='cookie-law-info-gdpr-css' href='http://blog.test/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-gdpr.css?ver=1.9.4' type='text/css' media='all' />
    <link rel='stylesheet' id='uncodefont-google-css' href='//fonts.googleapis.com/css?family=Abel%3Aregular%7CCairo%3A200%2C300%2Cregular%2C600%2C700%2C900&#038;subset=latin%2Clatin-ext%2Carabic&#038;ver=2.2.8' type='text/css' media='all' />
    <link rel="stylesheet" id="uncode-privacy-css" href="http://blog.test/wp-content/plugins/uncode-privacy/assets/css/uncode-privacy-public.css?ver=2.1.1" type="text/css" media="all" />
    <link rel="stylesheet" id="uncode-style-css" href="http://blog.test/wp-content/themes/blog/uncode/library/css/style.css?ver=142619648" type="text/css" media="all" />
    <style id='uncode-style-inline-css' type='text/css'>
        @media (min-width: 960px) {
            .limit-width {
                max-width: 1200px;
                margin: auto;
            }
        }

        @media (min-width: 960px) {

            .main-header,
            .vmenu-container {
                width: 252px;
            }
        }
    </style>
    <link rel="stylesheet" id="uncode-icons-css" href="http://blog.test/wp-content/themes/blog/uncode/library/css/uncode-icons.css?ver=142619648" type="text/css" media="all" />
    <link rel="stylesheet" id="uncode-custom-style-css" href="http://blog.test/wp-content/themes/blog/uncode/library/css/style-custom.css?ver=142619648" type="text/css" media="all" />
    <!-- <style id='uncode-custom-style-inline-css' type='text/css'>
        .firsthide {
            display: none;
        }

        #toggleview:active {
            background-color: #ebebeb;
            border: 3px solid white;
            color: black;
        }

        #hiddentext {
            display: none;
        }

        #menu-item-1215 a {
            text-indent: -15px;
        }

        #menu-item-1215 a:before {
            content: url(/wp-content/uploads/2020/08/FACCIA3Tavola-disegno-1MAppesmal.png);
            margin-right: 5px;
        }

        #menu-item-1215 a:active:before {
            content: url(/wp-content/uploads/2020/08/FACCIA3Tavola-disegno-1-copiaMAppesmal.png);
            margin-right: 5px;
        }

        #menu-item-1431 a:before {
            content: url(/wp-content/uploads/2020/08/FACCIA3Tavola-disegno-1-copia-2MAppesmal.png);
            margin-right: 5px;
        }

        #menu-item-1431 a:active:before {
            content: url(/wp-content/uploads/2020/08/FACCIA3Tavola-disegno-1-copia-3MAppesmal.png);
            margin-right: 5px;
        }

        #menu-item-1431 a {
            color: #ed1c24;
            text-indent: -15px;
        }
    </style> -->
    <link rel="stylesheet" id="child-style-css" href="http://blog.test/wp-content/themes/blog/uncode-child/style.css?ver=503663104" type="text/css" media="all" />
    <script type='text/javascript' src='http://blog.test/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
    <script type='text/javascript' src='http://blog.test/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var LS_Meta = {
            "v": "6.10.2"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://blog.test/wp-content/plugins/LayerSlider/static/layerslider/js/greensock.js?ver=1.19.0'></script>
    <script type='text/javascript' src='http://blog.test/wp-content/plugins/LayerSlider/static/layerslider/js/layerslider.kreaturamedia.jquery.js?ver=6.10.2'></script>
    <script type='text/javascript' src='http://blog.test/wp-content/plugins/LayerSlider/static/layerslider/js/layerslider.transitions.js?ver=6.10.2'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var Cli_Data = {
            "nn_cookie_ids": [],
            "cookielist": [],
            "ccpaEnabled": "",
            "ccpaRegionBased": "",
            "ccpaBarEnabled": "",
            "ccpaType": "gdpr",
            "js_blocking": "1",
            "custom_integration": "",
            "triggerDomRefresh": ""
        };
        var cli_cookiebar_settings = {
            "animate_speed_hide": "500",
            "animate_speed_show": "500",
            "background": "#FFF",
            "border": "#b1a6a6c2",
            "border_on": "",
            "button_1_button_colour": "#0a0606",
            "button_1_button_hover": "#080505",
            "button_1_link_colour": "#ffffff",
            "button_1_as_button": "1",
            "button_1_new_win": "",
            "button_2_button_colour": "#333",
            "button_2_button_hover": "#292929",
            "button_2_link_colour": "#444",
            "button_2_as_button": "",
            "button_2_hidebar": "",
            "button_3_button_colour": "#3566bb",
            "button_3_button_hover": "#2a5296",
            "button_3_link_colour": "#fff",
            "button_3_as_button": "1",
            "button_3_new_win": "",
            "button_4_button_colour": "#000",
            "button_4_button_hover": "#000000",
            "button_4_link_colour": "#333333",
            "button_4_as_button": "",
            "font_family": "inherit",
            "header_fix": "",
            "notify_animate_hide": "1",
            "notify_animate_show": "1",
            "notify_div_id": "#cookie-law-info-bar",
            "notify_position_horizontal": "right",
            "notify_position_vertical": "bottom",
            "scroll_close": "",
            "scroll_close_reload": "",
            "accept_close_reload": "",
            "reject_close_reload": "",
            "showagain_tab": "",
            "showagain_background": "#fff",
            "showagain_border": "#000",
            "showagain_div_id": "#cookie-law-info-again",
            "showagain_x_position": "100px",
            "text": "#333333",
            "show_once_yn": "",
            "show_once": "10000",
            "logging_on": "",
            "as_popup": "",
            "popup_overlay": "1",
            "bar_heading_text": "",
            "cookie_bar_as": "banner",
            "popup_showagain_position": "bottom-right",
            "widget_position": "left"
        };
        var log_object = {
            "ajax_url": "https:\/\/www.fulviobugani.com\/wp-admin\/admin-ajax.php"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://blog.test/wp-content/plugins/cookie-law-info/public/js/cookie-law-info-public.js?ver=1.9.4'></script>
    <script type='text/javascript' src='/wp-content/themes/blog/uncode/library/js/ai-uncode.js' id='uncodeAI' data-home='/' data-path='/' data-breakpoints-images='258,516,720,1032,1440,2064,2880'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var SiteParameters = {
            "days": "days",
            "hours": "hours",
            "minutes": "minutes",
            "seconds": "seconds",
            "constant_scroll": "on",
            "scroll_speed": "2",
            "parallax_factor": "0.25",
            "loading": "Loading\u2026",
            "slide_name": "slide",
            "slide_footer": "footer",
            "ajax_url": "https:\/\/www.fulviobugani.com\/wp-admin\/admin-ajax.php",
            "nonce_adaptive_images": "330e4773de",
            "enable_debug": "",
            "block_mobile_videos": "",
            "is_frontend_editor": "",
            "mobile_parallax_allowed": "",
            "wireframes_plugin_active": "1"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://blog.test/wp-content/themes/blog/uncode/library/js/init.js?ver=142619648'></script>
    <meta name="generator" content="Powered by LayerSlider 6.10.2 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress." />
    <!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
    <link rel='https://api.w.org/' href='https://www.fulviobugani.com/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.fulviobugani.com/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.fulviobugani.com/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.4.15" />
    <link rel="canonical" href="https://www.fulviobugani.com/waria/" />
    <link rel='shortlink' href='https://www.fulviobugani.com/?p=1451' />
    <link rel="alternate" type="application/json+oembed" href="https://www.fulviobugani.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.fulviobugani.com%2Fwaria%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://www.fulviobugani.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.fulviobugani.com%2Fwaria%2F&#038;format=xml" />
    <link rel="icon" href="http://blog.test/wp-content/uploads/2024/05/quang-nguyen-photo.jpg" sizes="32x32" />
    <link rel="icon" href="http://blog.test/wp-content/uploads/2024/05/quang-nguyen-photo.jpg" sizes="192x192" />
    <link rel="apple-touch-icon" href="http://blog.test/wp-content/uploads/2024/05/quang-nguyen-photo.jpg" />
    <meta name="msapplication-TileImage" content="http://blog.test/wp-content/uploads/2024/05/quang-nguyen-photo.jpg" />
    <!-- <style type="text/css" data-type="vc_shortcodes-custom-css">
        .vc_custom_1594645810434 {
            padding-top: 20px !important;
            padding-bottom: 20px !important;
        }
    </style> -->
    <noscript>
        <style>
            .wpb_animate_when_almost_visible {
                opacity: 1;
            }
        </style>
    </noscript>
</head>

<body class="home page-template-default page page-id-1102  style-color-lxmt-bg vmenu vmenu-middle vmenu-left vmenu-position-left header-full-width main-center-align menu-mobile-transparent menu-accordion-active mobile-parallax-not-allowed ilb-no-bounce uncode-logo-mobile wpb-js-composer js-comp-ver-6.1.0 vc_responsive" data-border="0">
    <div class="body-borders" data-border="0">
        <div class="top-border body-border-shadow"></div>
        <div class="right-border body-border-shadow"></div>
        <div class="bottom-border body-border-shadow"></div>
        <div class="left-border body-border-shadow"></div>
        <div class="top-border style-light-bg"></div>
        <div class="right-border style-light-bg"></div>
        <div class="bottom-border style-light-bg"></div>
        <div class="left-border style-light-bg"></div>
    </div>
    <div class="box-wrapper">
        <div class="box-container">
            <script type="text/javascript">
                UNCODE.initBox();
            </script>
            <div class="main-header">
                <div id="masthead" class="masthead-vertical">
                    <div class="vmenu-container menu-container  menu-primary menu-light submenu-light style-light-original style-color-xsdn-bg menu-no-borders menu-no-arrows">
                        <div class="row row-parent">
                            <div class="row-inner restrict row-brand">
                                <div id="logo-container-mobile" class="col-lg-12 logo-container">
                                    <div class="style-light">
                                        <div class="style-light">
                                            <a href="http://blog.test" class="navbar-brand" data-minheight="20">
                                                <div class="logo-image main-logo  logo-light" data-maxheight="20" style="height: 20px;"><img src="http://blog.test/wp-content/uploads/2024/05/quang-nguyen-photo.jpg" alt="logo" width="258" height="75" class="img-responsive adaptive-async" data-uniqueid="3457-188001" data-guid="http://blog.test/wp-content/uploads/2024/05/quang-nguyen-photo.jpg" data-path="2024/05/quang-nguyen-photo.jpg" data-width="69" data-height="20" data-singlew="null" data-singleh="null" data-crop="" data-fixed="height" /></div>
                                                <div class="logo-image mobile-logo logo-light" data-maxheight="20" style="height: 20px;"><img src="http://blog.test/wp-content/uploads/2024/05/quang-nguyen-photo.jpg" data-path="2024/05/quang-nguyen-photo.jpg" data-width="69" data-height="20" data-singlew="null" data-singleh="null" data-crop="" data-fixed="height" /></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mmb-container">
                                        <div class="mobile-menu-button 8 mobile-menu-button-light lines-button x2"><span class="lines"></span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-inner expand">
                                <div class="main-menu-container">
                                    <div class="vmenu-row-wrapper">
                                        <div class="vmenu-wrap-cell">
                                            <div class="row-inner expand">
                                                <div class="menu-sidebar navbar-main">
                                                    <hr>
                                                    <div class="menu-sidebar-inner">
                                                        <div class="menu-accordion">
                                                            <ul id="menu-newmenu" class="menu-primary-inner menu-smart sm sm-vertical">
                                                                <li id="menu-item-3492" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3464 dropdown menu-item-link"><a title="Galery" href="#" data-toggle="dropdown" class="dropdown-toggle" data-type="title">Galery<i class="fa fa-angle-down fa-dropdown"></i></a>
                                                                    <ul role="menu" class="drop-menu">
                                                                        <li id="menu-item-1460" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1460 dropdown"><a title="Art nude" href="#" data-type="title">Art nude<i class="fa fa-angle-down fa-dropdown"></i></a>
                                                                            <ul role="menu" class="drop-menu">
                                                                                <li id="menu-item-3910" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3910"><a title="Harmony of Nature" href="http://blog.test/harmony-of-nature/">Harmony of Nature<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2480" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2480"><a title="Body scapes" href="http://blog.test/body-scapes/">Body scapes<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                            </ul>
                                                                        </li>

                                                                        <li id="menu-item-1465" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1465 dropdown"><a title="Travel" href="#" data-type="title">Travel<i class="fa fa-angle-down fa-dropdown"></i></a>
                                                                            <ul role="menu" class="drop-menu">
                                                                                <li id="menu-item-2973" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2973"><a title="Cuba" href="http://blog.test/cuba/">Cuba<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2974" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2974"><a title="US" href="http://blog.test/us/">US<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2975" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2975"><a title="Myanmar" href="http://blog.test/myanmar/">Myanmar <i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2976" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2976"><a title="Japan" href="http://blog.test/japan/">Japan<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2977" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2977"><a title="Italy" href="http://blog.test/italy/">Italy<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2978" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2978"><a title="India" href="http://blog.test/india/">India<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2979" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2979"><a title="Vietnam" href="http://blog.test/vietnam/">Vietnam<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                            </ul>
                                                                        </li>

                                                                        <li id="menu-item-2600" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2600 dropdown"><a title="Architect " href="#" data-type="title">Architect<i class="fa fa-angle-down fa-dropdown"></i></a>
                                                                            <ul role="menu" class="drop-menu">
                                                                                <li id="menu-item-2601" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2601"><a title="Sydney opera" href="http://blog.test/sydney-opera/">Sydney opera<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2602" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2602"><a title="US Walt Disney concert hall" href="http://blog.test/us-walt-disney-concert-hall/">US Walt Disney concert hall<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2603" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2603"><a title="Korean" href="http://blog.test/korean/">Korean<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                            </ul>
                                                                        </li>

                                                                        <li id="menu-item-2988" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2988"><a title="Portrait" href="http://blog.test/portrait/">Portrait<i class="fa fa-angle-right fa-dropdown"></i></a></li>

                                                                        <li id="menu-item-2500" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2500 dropdown"><a title="Festivities" href="#" data-type="title">Festivities<i class="fa fa-angle-down fa-dropdown"></i></a>
                                                                            <ul role="menu" class="drop-menu">
                                                                                <li id="menu-item-2501" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2501"><a title="Nasir Ladakh" href="http://blog.test/nasir-ladakh/">Nasir Ladakh<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2502" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2502"><a title="Sicily" href="http://blog.test/sicily/">Sicily<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2503" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2503"><a title="India" href="http://blog.test/india-2/">India<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                                <li id="menu-item-2504" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2504"><a title="Cuba dance ?" href="http://blog.test/cuba-dance/">Cuba dance ?<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li id="menu-item-3483" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3483 menu-item-link"><a title="About" href="http://blog.test/about-me/">About<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                <li id="menu-item-3484" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3484 menu-item-link"><a title="Contact" href="http://blog.test/contact/">Contact<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                <li id="menu-item-1215" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1215 menu-item-link"><a title="Service" href="https://www.fulviobugani.com/cuba/">Product/Service<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                <li id="menu-item-1431" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1431 menu-item-link"><a title="Blog" href="https://www.fulviobugani.com/leica-camera/">Blog<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                <li id="menu-item-3485" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3485 menu-item-link"><a title="Accessibility" href="#">Accessibility<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                <li id="menu-item-3486" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3486 menu-item-link"><a title="Supporting Content" href="#">Supporting Content<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                <li id="menu-item-3487" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3487 menu-item-link"><a title="SEO checklist" href="#">SEO checklist<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                <li id="menu-item-3488" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3488 menu-item-link"><a title="Email Marketing" href="#">Email Marketing<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                <li id="menu-item-3489" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3489 menu-item-link"><a title="Tracking & Scripts" href="#">Tracking & Scripts<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                <li id="menu-item-3490" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3490 menu-item-link"><a title="eCommerce" href="#">eCommerce<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                <li id="menu-item-3491" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3491 menu-item-link"><a title="Legals" href="#">Legals<i class="fa fa-angle-right fa-dropdown"></i></a></li>

                                                                <!-- <li id="menu-item-3499" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3499 dropdown menu-item-link"><a title="Corporate" href="#" data-toggle="dropdown" class="dropdown-toggle" data-type="title">Corporate<i class="fa fa-angle-down fa-dropdown"></i></a>
                                                                    <ul role="menu" class="drop-menu">
                                                                        <li id="menu-item-3500" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3500"><a title="Leica M10" href="https://www.fulviobugani.com/leica-m10/">Leica M10<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                        <li id="menu-item-2921" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2921"><a title="Universal music" href="https://www.fulviobugani.com/universal-music/">Universal music<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                        <li id="menu-item-2922" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2922"><a title="Juventus" href="https://www.fulviobugani.com/juventus/">Juventus<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                        <li id="menu-item-3591" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3591"><a title="Lamborghini" href="https://www.fulviobugani.com/lamborghini/">Lamborghini<i class="fa fa-angle-right fa-dropdown"></i></a></li>
                                                                    </ul>
                                                                </li>
                                                                <li id="menu-item-1485" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1485 menu-item-link"><a title="Workshop" href="https://www.fulviobugani.com/newworkshop/">Workshop<i class="fa fa-angle-right fa-dropdown"></i></a></li> -->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row-inner restrict">
                                                <div class="menu-sidebar">
                                                    <div class="menu-sidebar-inner">
                                                        <!-- <div class="menu-accordion">
                                                            <ul id="menu-menu-secondario-1" class="menu-smart sm sm-vertical">
                                                                
                                                            </ul>
                                                        </div> -->
                                                        <div class="nav navbar-nav navbar-social">
                                                            <ul class="menu-smart sm menu-social mobile-hidden tablet-hidden">
                                                                <li class="menu-item-link social-icon tablet-hidden mobile-hidden social-954146"><a href="#" class="social-menu-link" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                                                <li class="menu-item-link social-icon tablet-hidden mobile-hidden social-710161"><a href="#" class="social-menu-link" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="mobile-hidden tablet-hidden vmenu-footer style-light">
                                                            <p>©Copyright 2024 – Quang Nguyen<br /><a href="http://blog.test/privacy-policy/">Privacy Policy</a> | <a href="http://blog.test/cookie-policy/">Cookie Policy</a></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                UNCODE.fixMenuHeight();
            </script>

            <div class="style-light footer-scroll-top"><a href="#" class="scroll-top"><i class="fa fa-angle-up fa-stack fa-rounded btn-default btn-hover-nobg"></i></a></div><!--googleoff: all-->
            <div id="cookie-law-info-bar" data-nosnippet="true"><span>
                    <div class="cli-bar-container cli-style-v2">
                        <div class="cli-bar-message">Questo sito fa uso di cookie per migliorare l’esperienza di navigazione degli utenti e per raccogliere informazioni sull’utilizzo del sito stesso. Proseguendo nella navigazione si accetta l’uso dei cookie; in caso contrario è possibile abbandonare il sito.</div>
                        <div class="cli-bar-btn_container"><a href='http://blog.test/cookie-policy/' id="CONSTANT_OPEN_URL" target="_blank" class="cli-plugin-main-link" style="display:inline-block; margin:0px 10px 0px 5px; ">Cookie Policy</a><a role='button' tabindex='0' data-cli_action="accept" id="cookie_action_close_header" class="medium cli-plugin-button cli-plugin-main-button cookie_action_close_header cli_action_button" style="display:inline-block; ">Accetto</a></div>
                    </div>
                </span></div>
            <div id="cookie-law-info-again" style="display:none;" data-nosnippet="true"><span id="cookie_hdr_showagain">Manage consent</span></div>
            <div class="cli-modal" data-nosnippet="true" id="cliSettingsPopup" tabindex="-1" role="dialog" aria-labelledby="cliSettingsPopup" aria-hidden="true">
                <div class="cli-modal-dialog" role="document">
                    <div class="cli-modal-content cli-bar-popup">
                        <button type="button" class="cli-modal-close" id="cliModalClose">
                            <svg class="" viewBox="0 0 24 24">
                                <path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path>
                                <path d="M0 0h24v24h-24z" fill="none"></path>
                            </svg>
                            <span class="wt-cli-sr-only">Close</span>
                        </button>
                        <div class="cli-modal-body">
                            <div class="cli-container-fluid cli-tab-container">
                                <div class="cli-row">
                                    <div class="cli-col-12 cli-align-items-stretch cli-px-0">
                                        <div class="cli-privacy-overview">
                                            <h4>Privacy Overview</h4>
                                            <div class="cli-privacy-content">
                                                <div class="cli-privacy-content-text">This website uses cookies to improve your experience while you navigate through the website. Out of these, the cookies that are categorized as necessary are stored on your browser as they are essential for the working of basic functionalities of the website. We also use third-party cookies that help us analyze and understand how you use this website. These cookies will be stored in your browser only with your consent. You also have the option to opt-out of these cookies. But opting out of some of these cookies may affect your browsing experience.</div>
                                            </div>
                                            <a class="cli-privacy-readmore" data-readmore-text="Show more" data-readless-text="Show less"></a>
                                        </div>
                                    </div>
                                    <div class="cli-col-12 cli-align-items-stretch cli-px-0 cli-tab-section-container">

                                        <div class="cli-tab-section">
                                            <div class="cli-tab-header">
                                                <a role="button" tabindex="0" class="cli-nav-link cli-settings-mobile" data-target="necessary" data-toggle="cli-toggle-tab">
                                                    Necessary </a>
                                                <div class="wt-cli-necessary-checkbox">
                                                    <input type="checkbox" class="cli-user-preference-checkbox" id="wt-cli-checkbox-necessary" data-id="checkbox-necessary" checked="checked" />
                                                    <label class="form-check-label" for="wt-cli-checkbox-necessary">Necessary</label>
                                                </div>
                                                <span class="cli-necessary-caption">Always Enabled</span>
                                            </div>
                                            <div class="cli-tab-content">
                                                <div class="cli-tab-pane cli-fade" data-id="necessary">
                                                    <p>Necessary cookies are absolutely essential for the website to function properly. This category only includes cookies that ensures basic functionalities and security features of the website. These cookies do not store any personal information.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="cli-tab-section">
                                            <div class="cli-tab-header">
                                                <a role="button" tabindex="0" class="cli-nav-link cli-settings-mobile" data-target="non-necessary" data-toggle="cli-toggle-tab">
                                                    Non-necessary </a>
                                                <div class="cli-switch">
                                                    <input type="checkbox" id="wt-cli-checkbox-non-necessary" class="cli-user-preference-checkbox" data-id="checkbox-non-necessary" checked='checked' />
                                                    <label for="wt-cli-checkbox-non-necessary" class="cli-slider" data-cli-enable="Enabled" data-cli-disable="Disabled"><span class="wt-cli-sr-only">Non-necessary</span></label>
                                                </div>
                                            </div>
                                            <div class="cli-tab-content">
                                                <div class="cli-tab-pane cli-fade" data-id="non-necessary">
                                                    <p>Any cookies that may not be particularly necessary for the website to function and is used specifically to collect user personal data via analytics, ads, other embedded contents are termed as non-necessary cookies. It is mandatory to procure user consent prior to running these cookies on your website.</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
            <div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
            <!--googleon: all-->
            <div class="gdpr-overlay"></div>
            <div class="gdpr gdpr-privacy-preferences">
                <div class="gdpr-wrapper">
                    <form method="post" class="gdpr-privacy-preferences-frm" action="https://www.fulviobugani.com/wp-admin/admin-post.php">
                        <input type="hidden" name="action" value="uncode_privacy_update_privacy_preferences">
                        <input type="hidden" id="update-privacy-preferences-nonce" name="update-privacy-preferences-nonce" value="6360e636fc" /><input type="hidden" name="_wp_http_referer" value="/about-me/" />
                        <header>
                            <div class="gdpr-box-title">
                                <h3>Privacy Preference Center</h3>
                                <span class="gdpr-close"></span>
                            </div>
                        </header>
                        <div class="gdpr-content">
                            <div class="gdpr-tab-content">
                                <div class="gdpr-consent-management gdpr-active">
                                    <header>
                                        <h4>Privacy Preferences</h4>
                                    </header>
                                    <div class="gdpr-info">
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer>
                            <input type="submit" class="btn-accent btn-flat" value="Save Preferences">
                        </footer>
                    </form>
                </div>
            </div>
            <script type="text/javascript">
                function myFunction() {
                    document.getElementById("myDropdown").classList.toggle("show");

                    var x = document.getElementById("myDropdown2");
                    if (x.style.display === "none") {
                        x.style.display = "block";
                    } else {
                        x.style.display = "none";
                    }

                }

                function myFunction2() {
                    document.getElementById("hiddentext").classList.toggle("show");


                }
            </script>
            <script type="text/html" id="wpb-modifications"></script>
            <script type='text/javascript' src='http://blog.test/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
            <script type='text/javascript'>
                /* <![CDATA[ */
                var DavesWordPressLiveSearchConfig = {
                    "resultsDirection": "",
                    "showThumbs": "false",
                    "showExcerpt": "false",
                    "displayPostCategory": "false",
                    "showMoreResultsLink": "true",
                    "activateWidgetLink": "true",
                    "minCharsToSearch": "0",
                    "xOffset": "0",
                    "yOffset": "0",
                    "blogURL": "https:\/\/www.fulviobugani.com",
                    "ajaxURL": "https:\/\/www.fulviobugani.com\/wp-admin\/admin-ajax.php",
                    "viewMoreText": "View more results",
                    "outdatedJQuery": "Dave's WordPress Live Search requires jQuery 1.2.6 or higher. WordPress ships with current jQuery versions. But if you are seeing this message, it's likely that another plugin is including an earlier version.",
                    "resultTemplate": "<ul id=\"dwls_search_results\" class=\"search_results dwls_search_results\" role=\"presentation\" aria-hidden=\"true\">\n<input type=\"hidden\" name=\"query\" value=\"<%- resultsSearchTerm %>\" \/>\n<% _.each(searchResults, function(searchResult, index, list) { %>\n        <%\n        \/\/ Thumbnails\n        if(DavesWordPressLiveSearchConfig.showThumbs == \"true\" && searchResult.attachment_thumbnail) {\n                liClass = \"post_with_thumb\";\n        }\n        else {\n                liClass = \"\";\n        }\n        %>\n        <li class=\"post-<%= searchResult.ID %> daves-wordpress-live-search_result <%- liClass %>\">\n\n        <a href=\"<%= searchResult.permalink %>\" class=\"daves-wordpress-live-search_title\">\n        <% if(DavesWordPressLiveSearchConfig.displayPostCategory == \"true\" && searchResult.post_category !== undefined) { %>\n                <span class=\"search-category\"><%= searchResult.post_category %><\/span>\n        <% } %><span class=\"search-title\"><%= searchResult.post_title %><\/span><\/a>\n\n        <% if(searchResult.post_price !== undefined) { %>\n                <p class=\"price\"><%- searchResult.post_price %><\/p>\n        <% } %>\n\n        <% if(DavesWordPressLiveSearchConfig.showExcerpt == \"true\" && searchResult.post_excerpt) { %>\n                <%= searchResult.post_excerpt %>\n        <% } %>\n\n        <% if(e.displayPostMeta) { %>\n                <p class=\"meta clearfix daves-wordpress-live-search_author\" id=\"daves-wordpress-live-search_author\">Posted by <%- searchResult.post_author_nicename %><\/p><p id=\"daves-wordpress-live-search_date\" class=\"meta clearfix daves-wordpress-live-search_date\"><%- searchResult.post_date %><\/p>\n        <% } %>\n        <div class=\"clearfix\"><\/div><\/li>\n<% }); %>\n\n<% if(searchResults[0].show_more !== undefined && searchResults[0].show_more && DavesWordPressLiveSearchConfig.showMoreResultsLink == \"true\") { %>\n        <div class=\"clearfix search_footer\"><a href=\"<%= DavesWordPressLiveSearchConfig.blogURL %>\/?s=<%-  resultsSearchTerm %>\"><%- DavesWordPressLiveSearchConfig.viewMoreText %><\/a><\/div>\n<% } %>\n\n<\/ul>\n"
                };
                /* ]]> */
            </script>
            <script type='text/javascript' src='http://blog.test/wp-content/plugins/uncode-daves-wordpress-live-search/js/daves-wordpress-live-search.js?ver=5.4.15'></script>
            <script type='text/javascript'>
                /* <![CDATA[ */
                var wpcf7 = {
                    "apiSettings": {
                        "root": "https:\/\/www.fulviobugani.com\/wp-json\/contact-form-7\/v1",
                        "namespace": "contact-form-7\/v1"
                    }
                };
                /* ]]> */
            </script>
            <script type='text/javascript' src='http://blog.test/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.9'></script>
            <script type='text/javascript' src='http://blog.test/wp-content/plugins/uncode-privacy/assets/js/js-cookie.min.js?ver=2.2.0'></script>
            <script type='text/javascript'>
                /* <![CDATA[ */
                var Uncode_Privacy_Parameters = {
                    "accent_color": "#0d669d"
                };
                /* ]]> */
            </script>
            <script type='text/javascript' src='http://blog.test/wp-content/plugins/uncode-privacy/assets/js/uncode-privacy-public.min.js?ver=2.1.1'></script>
            <script type='text/javascript'>
                var mejsL10n = {
                    "language": "en",
                    "strings": {
                        "mejs.download-file": "Download File",
                        "mejs.install-flash": "You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/",
                        "mejs.fullscreen": "Fullscreen",
                        "mejs.play": "Play",
                        "mejs.pause": "Pause",
                        "mejs.time-slider": "Time Slider",
                        "mejs.time-help-text": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.",
                        "mejs.live-broadcast": "Live Broadcast",
                        "mejs.volume-help-text": "Use Up\/Down Arrow keys to increase or decrease volume.",
                        "mejs.unmute": "Unmute",
                        "mejs.mute": "Mute",
                        "mejs.volume-slider": "Volume Slider",
                        "mejs.video-player": "Video Player",
                        "mejs.audio-player": "Audio Player",
                        "mejs.captions-subtitles": "Captions\/Subtitles",
                        "mejs.captions-chapters": "Chapters",
                        "mejs.none": "None",
                        "mejs.afrikaans": "Afrikaans",
                        "mejs.albanian": "Albanian",
                        "mejs.arabic": "Arabic",
                        "mejs.belarusian": "Belarusian",
                        "mejs.bulgarian": "Bulgarian",
                        "mejs.catalan": "Catalan",
                        "mejs.chinese": "Chinese",
                        "mejs.chinese-simplified": "Chinese (Simplified)",
                        "mejs.chinese-traditional": "Chinese (Traditional)",
                        "mejs.croatian": "Croatian",
                        "mejs.czech": "Czech",
                        "mejs.danish": "Danish",
                        "mejs.dutch": "Dutch",
                        "mejs.english": "English",
                        "mejs.estonian": "Estonian",
                        "mejs.filipino": "Filipino",
                        "mejs.finnish": "Finnish",
                        "mejs.french": "French",
                        "mejs.galician": "Galician",
                        "mejs.german": "German",
                        "mejs.greek": "Greek",
                        "mejs.haitian-creole": "Haitian Creole",
                        "mejs.hebrew": "Hebrew",
                        "mejs.hindi": "Hindi",
                        "mejs.hungarian": "Hungarian",
                        "mejs.icelandic": "Icelandic",
                        "mejs.indonesian": "Indonesian",
                        "mejs.irish": "Irish",
                        "mejs.italian": "Italian",
                        "mejs.japanese": "Japanese",
                        "mejs.korean": "Korean",
                        "mejs.latvian": "Latvian",
                        "mejs.lithuanian": "Lithuanian",
                        "mejs.macedonian": "Macedonian",
                        "mejs.malay": "Malay",
                        "mejs.maltese": "Maltese",
                        "mejs.norwegian": "Norwegian",
                        "mejs.persian": "Persian",
                        "mejs.polish": "Polish",
                        "mejs.portuguese": "Portuguese",
                        "mejs.romanian": "Romanian",
                        "mejs.russian": "Russian",
                        "mejs.serbian": "Serbian",
                        "mejs.slovak": "Slovak",
                        "mejs.slovenian": "Slovenian",
                        "mejs.spanish": "Spanish",
                        "mejs.swahili": "Swahili",
                        "mejs.swedish": "Swedish",
                        "mejs.tagalog": "Tagalog",
                        "mejs.thai": "Thai",
                        "mejs.turkish": "Turkish",
                        "mejs.ukrainian": "Ukrainian",
                        "mejs.vietnamese": "Vietnamese",
                        "mejs.welsh": "Welsh",
                        "mejs.yiddish": "Yiddish"
                    }
                };
            </script>
            <script type='text/javascript' src='http://blog.test/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.13-9993131'></script>
            <script type='text/javascript' src='http://blog.test/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.4.15'></script>
            <script type='text/javascript'>
                /* <![CDATA[ */
                var _wpmejsSettings = {
                    "pluginPath": "\/wp-includes\/js\/mediaelement\/",
                    "classPrefix": "mejs-",
                    "stretching": "responsive"
                };
                /* ]]> */
            </script>
            <script type='text/javascript' src='http://blog.test/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.4.15'></script>
            <script type='text/javascript' src='http://blog.test/wp-content/themes/blog/uncode/library/js/plugins.js?ver=1439317685'></script>
            <script type='text/javascript' src='http://blog.test/wp-content/themes/blog/uncode/library/js/app.js?ver=1439317685'></script>
            <script type='text/javascript' src='http://blog.test/wp-includes/js/wp-embed.min.js?ver=5.4.15'></script>