<?php
/*
Template Name: About Page
*/

get_header();
?>
<div class="main-wrapper">
    <div class="main-container">
        <div class="page-wrapper">
            <div class="sections-container">
                <script type="text/javascript">
                    UNCODE.initHeader();
                </script>
                <article id="post-1406" class="page-body style-light-bg post-1406 page type-page status-publish hentry">
                    <div class="post-wrapper">
                        <div class="post-body">
                            <div class="post-content un-no-sidebar-layout">
                                <section data-parent="true" class="row-container">
                                    <div class="row no-top-padding no-bottom-padding no-h-padding full-width row-parent">
                                        <div data-parent="true" class="vc_row row-container boomapps_vcrow">
                                            <div class="row limit-width row-parent">
                                                <div class="wpb_row row-inner">
                                                    <div class="wpb_column pos-top pos-center align_left column_parent col-lg-6 boomapps_vccolumn single-internal-gutter">
                                                        <div class="uncol style-light">
                                                            <div class="uncoltable">
                                                                <div class="uncell  boomapps_vccolumn no-block-padding">
                                                                    <div class="uncont">
                                                                        <div class="uncode-single-media  text-center">
                                                                            <div class="single-wrapper" style="max-width: 67%;">
                                                                                <div class="tmb tmb-light  img-circle tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg">
                                                                                    <div class="t-inside">
                                                                                        <div class="t-entry-visual">
                                                                                            <div class="t-entry-visual-tc">
                                                                                                <div class="uncode-single-media-wrapper img-circle">
                                                                                                    <img src="https://www.fulviobugani.com/wp-content/uploads/2015/12/fulvio_leicaQ.jpg" width="640" height="640" alt="Quang Nguyen photographer">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="uncode_text_column">
                                                                            <p>
                                                                                Oftentimes the best things in your life happen without plans, and for me becoming an amateur photographer was one of them. I have always appreciated the beautiful world of photography but have never had enough time to take it seriously.
                                                                                Since founding my IT company in 1996, my life had been preoccupied with the management of the company for the next twenty years. It wasn’t until five years ago when I first moved to Sydney, my love of photography has been rekindled, and I got my first Leica M camera.
                                                                                Since then, my passion for photography has been growing internally and I have begun to self-taught and tried to bring a camera wherever I travelled.
                                                                            </p>
                                                                            <style>
                                                                                p {
                                                                                    text-align: justify;
                                                                                }
                                                                            </style>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column pos-top pos-center align_left column_parent col-lg-6 boomapps_vccolumn single-internal-gutter">
                                                        <div class="uncol style-light">
                                                            <div class="uncoltable">
                                                                <div class="uncell  boomapps_vccolumn no-block-padding">
                                                                    <div class="uncont">
                                                                        <div class="uncode-accordion wpb_accordion wpb_content_element" data-collapsible="no" data-active-tab="1">
                                                                            <div class="panel-group wpb_wrapper wpb_accordion_wrapper" id="accordion_2053693403" role="tablist" aria-multiselectable="true">

                                                                                <div class="panel panel-default wpb_accordion_section group">
                                                                                    <div class="panel-heading wpb_accordion_header ui-accordion-header" role="tab">
                                                                                        <p class="panel-title active"><a data-toggle="collapse" data-parent="#accordion_2053693403" href="#1593965845-1-66"><span>Exhibition</span></a></p>
                                                                                    </div>
                                                                                    <div id="1593965845-1-66" class="panel-collapse collapse in" role="tabpanel">
                                                                                        <div class="panel-body wpb_accordion_content ui-accordion-content">
                                                                                            <div class="uncode_text_column">
                                                                                                <p>
                                                                                                    <strong>Vietnam’s smile</strong><br><br>
                                                                                                    <strong>Sydney 2019</strong><br><br>
                                                                                                    Vietnam, of course, is always on the top of my travelling list every year. I often return to the country five times a year and realized that I am still a stranger in my own homeland. There are so many amazing places that I have never been to.
                                                                                                    What’s fascinating is, despite many difficulties and challenges in life, Vietnamese people are always wreathed in smiles. These photographs are the time capsules of those beautiful moments and infectious smiles of the people I met in Vietnam; from the children on the highlands full of sunshine to the miners a few hundred meters deep down underground.<br><br>
                                                                                                    <strong>The life we missed Sydney 2022</strong>
                                                                                                </p>
                                                                                                <style>
                                                                                                    p {
                                                                                                        text-align: justify;
                                                                                                    }
                                                                                                </style>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default wpb_accordion_section group">
                                                                                    <div class="panel-heading wpb_accordion_header ui-accordion-header" role="tab">
                                                                                        <p class="panel-title"><a data-toggle="collapse" data-parent="#accordion_2053693403" href="#1593965845-2-21"><span>Award</span></a></p>
                                                                                    </div>
                                                                                    <div id="1593965845-2-21" class="panel-collapse collapse" role="tabpanel">
                                                                                        <div class="panel-body wpb_accordion_content ui-accordion-content">
                                                                                            <div class="uncode_text_column">
                                                                                                <ul>
                                                                                                    <li><a href="http://www.worldpressphoto.org/collection/photo/2015/contemporary-issues/fulvio-bugani" target="_blank" rel="noopener noreferrer">World Press Photo 2015</a> – “Contemporary issues” &#8211; 3° prize, single photo.</li>
                                                                                                    <li><a href="http://leica-oskar-barnack-preis.de/en/series-finalists/2016/fulvio-bugani.html" target="_blank" rel="noopener noreferrer">Leica Oskar Barnack Award 2016</a> &#8211; Finalist</li>
                                                                                                    <li><a href="http://m-magazine.photography/ceemes/article/show/1879/__language__=en" target="_blank" rel="noopener noreferrer">Leica Oskar Barnack Public Award 2016</a> &#8211; Winner</li>
                                                                                                    <li><a href="https://www.poy.org/76/08/02.html" target="_blank" rel="noopener noreferrer">POYi 2019</a> &#8211; 2° Prize, Portraits</li>
                                                                                                    <li><a href="http://www.shutterloveonline.com/contest_galleries/exposureaward/2018/#main" target="_blank" rel="noopener noreferrer">PDN Exposure Award 2018</a> &#8211; Winner</li>
                                                                                                    <li><a href="http://www.moscowfotoawards.com/winners/moscow/2018/4559/" target="_blank" rel="noopener noreferrer">MIFA – Moscow International Foto Award 2018</a> &#8211; 3° Prize, People Family</li>
                                                                                                    <li><a href="http://www.moscowfotoawards.com/winners/moscow/2018/4380/" target="_blank" rel="noopener noreferrer">MIFA – Moscow International Foto Award 2018</a> &#8211; 2° Prize, People Portrait</li>
                                                                                                    <li><a href="https://sipacontest.com/gallery/2018/category.php?cat=4" target="_blank" rel="noopener noreferrer">SIPA 2018</a> &#8211; Remarkable Award People</li>
                                                                                                    <li><a href="http://www.poyi.org/74/18/ae02.php">POYi 2017</a> &#8211; Award of Excellence Recreational Sports</li>
                                                                                                    <li><a href="http://monoawards.com/winners-gallery/monochrome-awards-2016/professional/people/gold-award" target="_blank" rel="noopener noreferrer">Monochrome Awards 2016</a> &#8211; 1° Prize, People</li>
                                                                                                    <li><a href="http://px3.fr/winners/zoom2.php?eid=1-56956-16" target="_blank" rel="noopener noreferrer">Px3 2016</a> &#8211; Bronze Winner &#8211; Nature/Environment</li>
                                                                                                    <li><a href="http://www.px3.fr/winners/public.php?compName=PX3+2016" target="_blank" rel="noopener noreferrer">Px3 2016 Public Award</a> &#8211; 2° and 3° Prize &#8211; Nature/Environment</li>
                                                                                                    <li>Head On Photo Awards 2018 &#8211; Finalist Mobile</li>
                                                                                                    <li><a href="https://px3.fr/winners/hm/2018/1-81347-18/" target="_blank" rel="noopener noreferrer">PX3 2018</a> &#8211; Honorable Mention Portraiture/Culture</li>
                                                                                                    <li><a href="http://www.moscowfotoawards.com/winners/hm/2017/10-13322-17/" target="_blank" rel="noopener noreferrer">MIFA &#8211; Moscow International Foto Award 2017</a> &#8211; Honorable mention General News</li>
                                                                                                    <li><a href="http://www.klphotoawards.com/#!FULVIO BUGANI Italy/zoom/vx816/dataItem-iok1ezhf1" target="_blank" rel="noopener noreferrer">Kuala Lampur International Photoaward 2016</a> &#8211; Finalist</li>
                                                                                                    <li><a href="https://issuu.com/kolga/docs/issue_1" target="_blank" rel="noopener noreferrer">Kolga Photo Awards 2016</a> &#8211; Finalist</li>
                                                                                                    <li><a href="http://monoawards.com/winners-gallery/monochrome-awards-2016/professional/fashion-beauty/hm/3430" target="_blank" rel="noopener noreferrer">Monochrome Awards 2016</a> &#8211; Honorable mention Fashion</li>
                                                                                                    <li><a href="http://monoawards.com/winners-gallery/monochrome-awards-2016/professional/photojournalism/hm/4878" target="_blank" rel="noopener noreferrer">Monochrome Awards 2016</a>&#8211; Honorable mention Photojournalism</li>
                                                                                                    <li><a href="http://px3.fr/winners/zoom2.php?eid=1-55908-16" target="_blank" rel="noopener noreferrer">Px3 Red 2016</a> &#8211; Honorable Mention</li>
                                                                                                    <li><a href="http://www.moscowfotoawards.com/winners/zoom.php?eid=10-7151-16" target="_blank" rel="noopener noreferrer">MIFA – Moscow International Foto Award 2016</a> &#8211; Honorable mention Portrait</li>
                                                                                                    <li><a href="http://www.moscowfotoawards.com/winners/zoom.php?eid=10-7149-16" target="_blank" rel="noopener noreferrer">MIFA – Moscow International Foto Award 2016</a> &#8211; Honorable mention Life Style</li>
                                                                                                    <li><a href="http://px3.fr/winners/zoom2.php?eid=1-56967-16" target="_blank" rel="noopener noreferrer">Px3 2016</a> &#8211; 2 Honorable Mentions Nature/Environment</li>
                                                                                                    <li><a href="http://www.photoawards.com/winner/zoom.php?eid=8-119869-16" target="_blank" rel="noopener noreferrer">IPA 2016</a> &#8211; Honorable Mention/Lifestyle</li>
                                                                                                    <li><a href="https://www.sipacontest.com/gallery/2016/category.php?cat=8" target="_blank" rel="noopener noreferrer">SIPA 2016</a> &#8211; Remarkable Award Sport</li>
                                                                                                    <li><a href="http://grant.photogrvphy.com/winners-gallery/photogrvphy-grant-2016/people/hm/162" target="_blank" rel="noopener noreferrer">Photography Grant</a> &#8211; Nominee People</li>
                                                                                                    <li><a href="http://grant.photogrvphy.com/winners-gallery/photogrvphy-grant-2016/story/hm/234" target="_blank" rel="noopener noreferrer">Photography Grant</a> &#8211; Nominee Story</li>
                                                                                                    <li><a href="https://www.headon.com.au/head-portrait-prize-2016-semi-finalists" target="_blank" rel="noopener noreferrer">Head On Portrait Prize 2016</a> &#8211; Shortlisted</li>
                                                                                                    <li>Nominated for the <a href="http://www.havana-fellowship.com/" target="_blank" rel="noopener noreferrer">Elliott Erwitt Havana Club 7 Fellowship</a></li>
                                                                                                    <li><a href="http://moscowfotoawards.com/winners/zoom.php?eid=10-4889-15" target="_blank" rel="noopener noreferrer">MIFA – Moscow International Foto Award 2015</a> &#8211; Honorable mention Portrait</li>
                                                                                                    <li><a href="http://moscowfotoawards.com/winners/zoom.php?eid=10-4890-15" target="_blank" rel="noopener noreferrer">MIFA – Moscow International Foto Award 2015</a> &#8211; Honorable mention Life Style</li>
                                                                                                    <li><a href="http://monoawards.com/winners-gallery/monochrome-awards-2015/professional/photojournalism/hm/2457" target="_blank" rel="noopener noreferrer">Monochrome Awards</a> &#8211; Honorable mention Photojournalism</li>
                                                                                                    <li><a href="https://www.lensculture.com/2015-lensculture-portrait-award-winners?modal=true&amp;modal_type=winner&amp;modal_competition_id=portrait-awards-2015&amp;modal_winner_id=fulvio-bugani" target="_blank" rel="noopener noreferrer">Lens Culture Portrait Award 2015</a> – Finalist</li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default wpb_accordion_section group">
                                                                                    <div class="panel-heading wpb_accordion_header ui-accordion-header" role="tab">
                                                                                        <p class="panel-title"><a data-toggle="collapse" data-parent="#accordion_2053693403" href="#1593966791254-2-4"><span>Interview</span></a></p>
                                                                                    </div>
                                                                                    <div id="1593966791254-2-4" class="panel-collapse collapse" role="tabpanel">
                                                                                        <div class="panel-body wpb_accordion_content ui-accordion-content">
                                                                                            <div class="uncode_text_column">
                                                                                                <ul>
                                                                                                    <li><a href="https://ulubionykiosk.pl/demo/52-digital-camera-polska/2071" target="_blank" rel="noopener noreferrer">Digital Camera Polska</a> n°4/2015 (pages 100 &#8211; 101) &#8211; in Polish</li>
                                                                                                    <li><a href="https://birdinflight.com/inspiration/experience/fulvio-bugani-my-first-interest-is-to-document-a-situation-without-judging.html" target="_blank" rel="noopener noreferrer">Bird in Flight</a> &#8211; in English</li>
                                                                                                    <li><a href="https://birdinflight.com/ru/vdohnovenie/opyt/fulvio-bugani-mne-interesno-zafiksirovat-sobytie-a-ne-sudit-ego.html" target="_blank" rel="noopener noreferrer">Bird in Flight</a> &#8211; in Russian</li>
                                                                                                    <li><a href="http://www.photosophia.it/pdf/Photosophia12.pdf" target="_blank" rel="noopener noreferrer">Photosophia</a> n°12/2015 (pages 27 &#8211; 39) &#8211; in Italian</li>
                                                                                                    <li><a href="http://www.photographerswithoutborders.org/blog/2015/12/1/inside-indonesias-school-for-transgender-women" target="_blank" rel="noopener noreferrer">Photographers Without Borders</a> &#8211; in English</li>
                                                                                                    <li><a href="https://www.lensculture.com/articles/fulvio-bugani-waria-transgendered-communities" target="_blank" rel="noopener noreferrer">LensCultures</a> &#8211; in English</li>
                                                                                                    <li><a href="http://bologna.repubblica.it/cronaca/2015/02/21/news/fulvio_bugani_islamici_e_transessuali_un_sorriso_cos_nata_la_mia_foto_da_premio-107870984/" target="_blank" rel="noopener noreferrer">La Repubblica Bologna</a> &#8211; in Italian</li>
                                                                                                    <li><a href="http://www.radiocittafujiko.it/fulvio-bugani-il-fotografo-senza-filtri" target="_blank" rel="noopener noreferrer">Radio Città Fujiko</a> &#8211; in Italian</li>
                                                                                                    <li><a href="http://www.radiocittadelcapo.it/archives/freelance-bolognese-vince-il-world-press-photo-156005/" target="_blank" rel="noopener noreferrer">Radio Città del Capo</a> &#8211; audio &#8211; in Italian</li>
                                                                                                    <li><a href="https://www.mixcloud.com/pictures_of_you/picturesofyou-iii-stagione-world-press-photo-on-air-17-02-2015/" target="_blank" rel="noopener noreferrer">Poliradio</a> &#8211; Picture of you (min 1:13) &#8211; in Italian</li>
                                                                                                    <li><a href="http://ilmanifesto.info/la-sfida-transgender-di-shiva/" target="_blank" rel="noopener noreferrer">Il Manifesto</a> &#8211; in Italian</li>
                                                                                                    <li><a href="http://urbanpost.it/world-press-photo-2015-intervista-esclusiva-fulvio-bugani-uno-dei-vincitori-italiani" target="_blank" rel="noopener noreferrer">Urban Post</a> &#8211; in Italian</li>
                                                                                                    <li><a href="http://soulvu.com/post/121695608501/fulvio-bugani-photographs-indonesian-transgender" target="_blank" rel="noopener noreferrer">SoulVu Magazine</a> &#8211; in English</li>
                                                                                                    <li><a href="http://www.bibliotecafotografica.it/wordpress/?p=324" target="_blank" rel="noopener noreferrer">Biblioteca fotografica</a> &#8211; in Italian</li>
                                                                                                    <li><a href="http://www.larengodelviaggiatore.info/2009/08/dentro-kibera/" target="_blank" rel="noopener noreferrer">L&#8217;Arengo del Viaggiatore</a> n°62/2009 &#8211; in Italian</li>
                                                                                                    <li><a href="http://www.photographerswithoutborders.org/blog-content/?author=565df43be4b0f66de4245aea" target="_blank" rel="noopener noreferrer">Photographers Without Borders</a> &#8211; in English</li>
                                                                                                    <li><a href="https://lfi-online.de/ceemes/en/blog/video-fulvio-bugani-904.html" target="_blank" rel="noopener noreferrer">LFI 1/2016</a> &#8211; in English</li>
                                                                                                    <li><a href="http://www.leica-oskar-barnack-award.com/en/information/finalists-2016/fulvio-bugani.html" target="_blank" rel="noopener noreferrer">Leica Oskar Barnack Award 2016</a> &#8211; in English</li>
                                                                                                    <li><a href="https://lfi-online.de/ceemes/en/news/loba-2016-fulvio-bugani-1003986.html" target="_blank" rel="noopener noreferrer">Leica Fotografie International News</a> &#8211; in English</li>
                                                                                                    <li><a href="http://blog.leica-camera.com/2017/01/18/off-cuba-leica-m10/" target="_blank" rel="noopener noreferrer">The Leica Camera Blog</a> &#8211; Off to Cuba with a Leica M10 &#8211; in English</li>
                                                                                                    <li><a href="https://www.youtube.com/watch?v=DJoW89bDNCw" target="_blank" rel="noopener noreferrer">Leica Camera Youtube</a> &#8211; in English</li>
                                                                                                    <li><a href="https://lfi-online.de/ceemes/en/blog/one-photo-one-story-fulvio-bugani-1219.html" target="_blank" rel="noopener noreferrer">Leica Fotografie International</a> &#8211; One photo One story &#8211; In English</li>
                                                                                                    <li><a href="https://matca.vn/en/sao-cu-mai-quan-quanh-trong-nhung-quy-tac/" target="_blank" rel="noopener noreferrer">Matca</a> &#8211; interview in English</li>
                                                                                                    <li><a href="https://matca.vn/sao-cu-mai-quan-quanh-trong-nhung-quy-tac/" target="_blank" rel="noopener noreferrer">Matca</a> &#8211; interview in Vietnamese</li>
                                                                                                    <li><a href="https://matca.vn/en/leica-master-class-voi-fulvio-bugani-choi-cung-anh-sang/" target="_blank" rel="noopener noreferrer">Matca</a> &#8211; Workshop in English</li>
                                                                                                    <li><a href="https://www.nst.com.my/lifestyle/bots/2017/09/281081/lights-camera-emotions" target="_blank" rel="noopener noreferrer">New Straits Time</a> &#8211; in English</li>
                                                                                                    <li><a href="http://hanoimoi.com.vn/Tin-tuc/Van-hoa/876831/nhiep-anh-gia-noi-tieng-nguoi-y---fulvio-bugani-fotoimage-den-viet-nam" target="_blank" rel="noopener noreferrer">Hanoimoi</a> &#8211; in Vietnamese</li>
                                                                                                    <li><a href="http://vnreview.vn/tin-tuc-xa-hoi-so/-/view_content/content/2263579/leica-lan-dau-tien-mo-lop-hoc-nhiep-anh-tai-viet-nam" target="_blank" rel="noopener noreferrer">Vn Review</a> &#8211; in Vietnamese</li>
                                                                                                    <li><a href="http://english.vov.vn/culture/exhibition-shows-cuba-through-the-lens-367705.vov" target="_blank" rel="noopener noreferrer">The Leica Camera Blog</a> &#8211; Yo Soy Fidel &#8211; In English</li>
                                                                                                    <li><a href="http://english.vov.vn/culture/exhibition-shows-cuba-through-the-lens-367705.vov" target="_blank" rel="noopener noreferrer">The Voice of Vietnam</a> &#8211; in English</li>
                                                                                                    <li><a href="https://youtu.be/BZf6ZbNjrJQ" target="_blank" rel="noopener noreferrer">TH.US &#8211; Fulvio Bugani e la Street Photography</a> &#8211; In Italian</li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default wpb_accordion_section group">
                                                                                    <div class="panel-heading wpb_accordion_header ui-accordion-header" role="tab">
                                                                                        <p class="panel-title"><a data-toggle="collapse" data-parent="#accordion_2053693403" href="#1594568076544-3-3"><span>Publication</span></a></p>
                                                                                    </div>
                                                                                    <div id="1594568076544-3-3" class="panel-collapse collapse" role="tabpanel">
                                                                                        <div class="panel-body wpb_accordion_content ui-accordion-content">
                                                                                            <div class="uncode_text_column">
                                                                                                <ul>
                                                                                                    <li>Marie Claire &#8211; Unschooling &#8211; In Italian (September 2019)</li>
                                                                                                    <li>&#8220;Einblicke Weltweit: 100 Years of Waldorf Education&#8221; &#8211; Book (2019)</li>
                                                                                                    <li>Playboy Germany &#8211; publication of the work &#8220;The fishermen of Lamalera&#8221;(Issue 01/17)</li>
                                                                                                    <li><a href="http://english.vov.vn/culture/exhibition-shows-cuba-through-the-lens-367705.vov" target="_blank" rel="noopener noreferrer">The Leica Camera Blog</a> &#8211; Yo Soy Fidel &#8211; In English</li>
                                                                                                    <li><a href="https://lfi-online.de/ceemes/en/magazine/lfi-magazine/lfi-english-2017/02.html" target="_blank" rel="noopener noreferrer">LFI Magazine</a> &#8211; publication of the work &#8220;Cuba with Leica M10&#8221; (Issue 02/17)</li>
                                                                                                    <li><a href="https://www.theguardian.com/global-development-professionals-network/gallery/2017/mar/03/tusheti-people-georgia-in-pictures" target="_blank" rel="noopener noreferrer">The Guardian</a> &#8211; publication of the work “Twilight” (March 2017)</li>
                                                                                                    <li><a href="http://www.dailymail.co.uk/news/article-4190780/Inside-hidden-world-Indonesia-s-transgender-women.html" target="_blank" rel="noopener noreferrer">Daily Mail UK</a> &#8211; publication of work &#8220;Waria Portraits&#8221; (February 2017)</li>
                                                                                                    <li><a href="https://lfi-online.de/ceemes/en/magazine/specials-issues/leica-oskar-barnack-award-2016.html#magazine-preview" target="_blank" rel="noopener noreferrer">LFI Special Edition</a> &#8211; publication of the work &#8220;Soul y Sombras&#8221; (September 2016)</li>
                                                                                                    <li><a href="http://www.featureshoot.com/2017/01/powerful-look-inside-indonesias-transgender-community/">Featureshoot.com</a> &#8211; publication of work &#8220;Waria Portraits&#8221; (January 2017)</li>
                                                                                                    <li><a href="https://zekemagazine.atavist.com/cuba" target="_blank" rel="noopener noreferrer">Zeke Magazine</a> &#8211; publication of the work &#8220;Soul y Sombras&#8221; (Fall 2016)</li>
                                                                                                    <li><a href="http://www.6mois.fr/Elle-est-elle-meme?lang=fr" target="_blank" rel="noopener noreferrer">6Mois</a> &#8211; online publication of “Waria: being a different Muslim” (October 2016)</li>
                                                                                                    <li>Playboy Germany &#8211; publication of the work “Fisherman of Lamalera” (December &#8217;16)</li>
                                                                                                    <li><a href="http://time.com/3753080/indonesia-transgender-muslim-islam/" target="_blank" rel="noopener noreferrer">TIME LightBox</a> &#8211; publication of the work “Waria: being a different Muslim” (April &#8217;15)</li>
                                                                                                    <li><a href="https://lfi-online.de/ceemes/en/blog/video-fulvio-bugani-904.html" target="_blank" rel="noopener noreferrer">LFI Magazine</a> &#8211; publication of the work “Twilight” (Issue 01/16)</li>
                                                                                                    <li><a href="http://roadsandkingdoms.com/2015/the-men-with-the-bamboo-harpoons/" target="_blank" rel="noopener noreferrer">Roads &amp; Kingdoms</a> &#8211; publication of the work “Fisherman of Lamalera” (December &#8217;15)</li>
                                                                                                    <li><a href="http://www.trabajadores.cu/20160103/las-calles-de-oro-de-hialeah/" target="_blank" rel="noopener noreferrer">Trabajadores</a> &#8211; pubblicazione &#8216;Las calles de oro de Hialeah&#8217;</li>
                                                                                                    <li><a href="http://www.cubadebate.cu/especiales/2016/01/04/las-calles-de-oro-de-hialeah-un-ensayo-fotografico-sobre-la-emigracion/#.Vo0lY5PhC-o" target="_blank" rel="noopener noreferrer">Cubadebate</a> &#8211; pubblicazione &#8216;Las calles de oro de Hialeah&#8217;</li>
                                                                                                    <li><a href="https://birdinflight.com/inspiration/experience/fulvio-bugani-my-first-interest-is-to-document-a-situation-without-judging.html" target="_blank" rel="noopener noreferrer">Bird in Flight</a> &#8211; publication of some works (June &#8217;15)</li>
                                                                                                    <li><a href="https://ulubionykiosk.pl/demo/52-digital-camera-polska/2071" target="_blank" rel="noopener noreferrer">Digital Camera Polska</a> (Issue 4/2015 ; pages 100 – 101)</li>
                                                                                                    <li>Tourbook &#8211; World Tour Zucchero Sugar Fornaciari &#8220;La Sesion Cubana&#8221; (2013)</li>
                                                                                                    <li>Universal Music &#8211; CD-Book &#8220;Una Rosa Blanca&#8221; Zucchero Sugar Fornaciari (December &#8217;13)</li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <script id="script-162758" data-row="script-162758" type="text/javascript" class="vc_controls">
                                                        UNCODE.initRow(document.getElementById("script-162758"));
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <div data-parent="true" class="vc_row row-container boomapps_vcrow">
                                    <div class="row quad-top-padding quad-bottom-padding single-h-padding limit-width row-parent">
                                        <div class="wpb_row row-inner">
                                            <div class="wpb_column pos-top pos-center align_center column_parent col-lg-12 boomapps_vccolumn double-internal-gutter">
                                                <div class="uncol style-light">
                                                    <div class="uncoltable">
                                                        <div class="uncell  boomapps_vccolumn no-block-padding">
                                                            <div class="uncont">
                                                                <div class="vc_row row-internal row-container boomapps_vcrow">
                                                                    <div class="row row-child">
                                                                        <div class="wpb_row row-inner">
                                                                            <div class="wpb_column pos-top pos-center align_center column_child col-lg-12 boomapps_vccolumn half-internal-gutter">
                                                                                <div class="uncol style-light">
                                                                                    <div class="uncoltable">
                                                                                        <div class="uncell  boomapps_vccolumn no-block-padding">
                                                                                            <div class="uncont" style="max-width:696px;">
                                                                                                <div class="vc_custom_heading_wrap ">
                                                                                                    <div class="heading-text el-text">
                                                                                                        <h3 class="h1"><span>Tearsheet</span></h3>
                                                                                                    </div>
                                                                                                    <div class="clear"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="owl-carousel-wrapper carousel-overflow-visible carousel-not-active-opacity carousel-animation-first">
                                                                    <div class="owl-carousel-container owl-carousel-loading double-gutter">
                                                                        <div id="index-3255486526" class="owl-carousel owl-element owl-height-auto owl-dots-db-space owl-dots-outside owl-dots-single-block-padding owl-dots-align-center" data-loop="true" data-dots="true" data-dotsmobile="true" data-navmobile="false" data-navspeed="700" data-autoplay="false" data-stagepadding="0" data-lg="3" data-md="2" data-sm="1" data-vp-height="false">
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1612 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/lfi-leica-fotografie-international/" class="pushed" target="_self" data-lb-index="0">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/LFI-tearsheet-1.jpg" width="720" height="960" alt="Pubblication on LFI" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/lfi-leica-fotografie-international/" target="_self">LFI -leica Fotografie International</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1608 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/zeke-magazine/" class="pushed" target="_self" data-lb-index="1">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2017/01/Zeke-Magazine-Bugani-uai-303x404.jpg" width="303" height="404" alt="Zeke Magazine Cuba" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/zeke-magazine/" target="_self">Zeke Magazine</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1606 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.5%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/world-press-photo-tearsheet/" class="pushed" target="_self" data-lb-index="2">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/elPais-uai-630x841.png" width="630" height="841" alt="Bugani World Press Photo" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/world-press-photo-tearsheet/" target="_self">World Press Photo Tearsheet</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1604 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.4%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/trabajadores/" class="pushed" target="_self" data-lb-index="3">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/articolo-trabajadores1-uai-625x834.jpg" width="625" height="834" alt="tabajadores reportage Bugani" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/trabajadores/" target="_self">Trabajadores</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1601 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.5%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/time-lightbox/" class="pushed" target="_self" data-lb-index="4">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/TimeLightbox9-uai-475x634.jpg" width="475" height="634" alt="Time Lightbox reportage Shinta" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/time-lightbox/" target="_self">Time Lightbox</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1599 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.5%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/the-guardian/" class="pushed" target="_self" data-lb-index="5">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2017/04/The-guardian-uai-352x470.jpg" width="352" height="470" alt="Pubblicazione Tusheti the Guardian" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/the-guardian/" target="_self">The Guardian</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1596 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.4%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/soulvu-magazine/" class="pushed" target="_self" data-lb-index="6">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2017/01/Screenshot-2016-04-15-19.55.42-uai-613x818.png" width="613" height="818" alt="Fulvio Bugani" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/soulvu-magazine/" target="_self">Soulvu Magazine</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1592 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/roads-kingdoms/" class="pushed" target="_self" data-lb-index="7">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/RoadsKingdom1-uai-444x592.jpg" width="444" height="592" alt="Bugani Fishermen of Lamalera" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/roads-kingdoms/" target="_self">Roads &#038; Kingdoms</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1589 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/quality-quarterly/" class="pushed" target="_self" data-lb-index="8">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2018/06/QQ-2018-francesco-rossi-uai-534x712.jpg" width="534" height="712" alt="" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/quality-quarterly/" target="_self">Quality Quarterly</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1585 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.4%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/photographer-without-borders/" class="pushed" target="_self" data-lb-index="9">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/photographers-without-borders1-uai-631x842.png" width="631" height="842" alt="Bugani photographers without borders" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/photographer-without-borders/" target="_self">Photographer Without Borders</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1582 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/movimento-5-stelle/" class="pushed" target="_self" data-lb-index="10">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/Beppe-Grillo5-uai-450x600.jpg" width="450" height="600" alt="election campaign posters" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/movimento-5-stelle/" target="_self">Movimento 5 Stelle</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1578 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.4%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/marie-claire/" class="pushed" target="_self" data-lb-index="11">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2019/10/UNSCHOOLING-Marie-Claire-Italia-1-uai-574x766.jpg" width="574" height="766" alt="" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/marie-claire/" target="_self">Marie Claire</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1575 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/lfi-2017/" class="pushed" target="_self" data-lb-index="12">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2017/04/copertina-uai-774x1032.jpg" width="774" height="1032" alt="copertina LFI 2/2017" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/lfi-2017/" target="_self">LFI 2017</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1571 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.5%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/lfi-oskar-barnack-award/" class="pushed" target="_self" data-lb-index="13">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2017/01/LFI-Fulvio-Bugani-Loba-uai-562x750.jpg" width="562" height="750" alt="LFI Bugani Leica Oscar Barnack" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/lfi-oskar-barnack-award/" target="_self">Lfi &#8211; Oskar Barnack Award</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1565 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.5%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/lensculture-portrait-award-catalogue/" class="pushed" target="_self" data-lb-index="14">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2017/02/portrait-awards-2015-annual-50-uai-505x674.jpg" width="505" height="674" alt="Fulvio Bugani waria potraits" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/lensculture-portrait-award-catalogue/" target="_self">LensCulture Portrait Award Catalogue</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1562 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/kolga-awards-catalog-2016/" class="pushed" target="_self" data-lb-index="15">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/06/Kolga-twilight-portraits-uai-402x536.png" width="402" height="536" alt="Kolga Photo Award 2016" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/kolga-awards-catalog-2016/" target="_self">Kolga Awards Catalog 2016</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1560 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/inspiration-leica-akademie/" class="pushed" target="_self" data-lb-index="16">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2020/05/Inspiration-Leica-Akademie-Merten_teaser-960x640-uai-480x640.png" width="480" height="640" alt="Fulvio Bugani Leica Akademie book" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/inspiration-leica-akademie/" target="_self">Inspiration Leica Akademie</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1557 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/il-manifesto/" class="pushed" target="_self" data-lb-index="17">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/Manifesto1-uai-747x996.jpg" width="747" height="996" alt="Bugani on il manifesto" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/il-manifesto/" target="_self">Il Manifesto</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1553 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.4%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/featureshoot/" class="pushed" target="_self" data-lb-index="18">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2017/01/Featureshoot-waria-Bugani1-uai-635x847.jpg" width="635" height="847" alt="transgender portrait photo Bugani" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/featureshoot/" target="_self">Featureshoot</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1548 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.5%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/doctor-without-borders/" class="pushed" target="_self" data-lb-index="19">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/Doctor-without-border7-uai-421x562.jpg" width="421" height="562" alt="Bugani Doctors Without borders" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/doctor-without-borders/" target="_self">Doctor Without Borders</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1542 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.4%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/digital-camera-polska/" class="pushed" target="_self" data-lb-index="20">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/Digital-Camera-Polska-uai-380x507.png" width="380" height="507" alt="Bugani on Digital Camera Polska" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/digital-camera-polska/" target="_self">Digital Camera Polska</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1538 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.4%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/daily-mail-uk/" class="pushed" target="_self" data-lb-index="21">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2017/02/Daily-mail-Bugani1-uai-542x723.jpg" width="542" height="723" alt="" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/daily-mail-uk/" target="_self">Daily Mail UK</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1534 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.4%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/cubadebate/" class="pushed" target="_self" data-lb-index="22">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/Cubadebate-reportage-Bugani4-uai-566x755.jpg" width="566" height="755" alt="Reportage Cubadebate" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/cubadebate/" target="_self">Cubadebate</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1528 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/boots/" class="pushed" target="_self" data-lb-index="23">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2017/11/copertina-uai-720x960.jpg" width="720" height="960" alt="" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/boots/" target="_self">Boots</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1435 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.5%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/bird-in-flight/" class="pushed" target="_self" data-lb-index="24">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/Bird-in-flight22-uai-400x534.jpg" width="400" height="534" alt="Bugani Bird in flight" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/bird-in-flight/" target="_self">Bird In Flight</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1426 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.3%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/amnesty-international/" class="pushed" target="_self" data-lb-index="25">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2016/04/Screenshot-2018-05-16-15.16.21-uai-612x816.png" width="612" height="816" alt="" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/amnesty-international/" target="_self">Amnesty International</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tmb tmb-carousel tmb-iso-h33 tmb-light tmb-overlay-text-anim tmb-content-center tmb-content-mobile-center tmb-image-anim  grid-cat-12 tmb-id-1422 tmb-img-ratio tmb-content-under tmb-media-first tmb-no-bg">
                                                                                <div class="t-inside animate_when_almost_visible zoom-in" data-delay="200">
                                                                                    <div class="t-entry-visual">
                                                                                        <div class="t-entry-visual-tc">
                                                                                            <div class="t-entry-visual-cont">
                                                                                                <div class="dummy" style="padding-top: 133.4%;"></div><a tabindex="-1" href="https://www.fulviobugani.com/6mois/" class="pushed" target="_self" data-lb-index="26">
                                                                                                    <div class="t-entry-visual-overlay">
                                                                                                        <div class="t-entry-visual-overlay-in style-color-wayh-bg" style="opacity: 0.5;"></div>
                                                                                                    </div>
                                                                                                    <div class="t-overlay-wrap">
                                                                                                        <div class="t-overlay-inner">
                                                                                                            <div class="t-overlay-content">
                                                                                                                <div class="t-overlay-text single-block-padding">
                                                                                                                    <div class="t-entry t-single-line"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><img src="https://www.fulviobugani.com/wp-content/uploads/2017/01/Shinta-500-uai-338x451.jpg" width="338" height="451" alt="6 Mois Portrait Fulvio Bugani" />
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="t-entry-text">
                                                                                        <div class="t-entry-text-tc single-block-padding">
                                                                                            <div class="t-entry">
                                                                                                <h3 class="t-entry-title h4"><a href="https://www.fulviobugani.com/6mois/" target="_self">6mois</a></h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <script id="script-290621" data-row="script-290621" type="text/javascript" class="vc_controls">
                                                UNCODE.initRow(document.getElementById("script-290621"));
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div><!-- sections container -->
        </div><!-- page wrapper -->
        <!-- <footer id="colophon" class="site-footer">
            <div class="footer-content-block row-container  style-light-bg style-light">
                <div class="footer-content-block-inner limit-width row-parent"></div>
            </div>
            <div class="row-container style-dark-bg footer-last desktop-hidden">
                <div class="row row-parent style-dark limit-width no-top-padding no-h-padding no-bottom-padding">
                    <div class="site-info uncell col-lg-6 pos-middle text-left">
                        <p>©Copyright 2020 – Fulvio Bugani<br /><a href="http://www.fulviobugani.com/privacy-policy/">Privacy Policy</a> | <a href="http://www.fulviobugani.com/cookie-policy/">Cookie Policy</a></p>
                    </div>os-middle text-right">
                        <div class="social-icon icon-box icon-box-top icon-inline"><a href="https://www.facebook.com/FulvioBuganiFotoimage/" target="_blank"><i class="fa fa-facebook-square"></i></a></div>
                        <div class="social-icon icon-box icon-box-top icon-inline"><a href="https://www.instagram.com/fulviobugani_fotoimage/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></div>
                    </div>
                </div>
            </div>
        </footer> -->
    </div><!-- main container -->
</div><!-- main wrapper -->
</div><!-- box container -->
</div><!-- box wrapper -->
<div class="style-light footer-scroll-top"><a href="#" class="scroll-top"><i class="fa fa-angle-up fa-stack fa-rounded btn-default btn-hover-nobg"></i></a></div><!--googleoff: all-->
<div id="cookie-law-info-bar" data-nosnippet="true"><span>
        <div class="cli-bar-container cli-style-v2">
            <div class="cli-bar-message">Questo sito fa uso di cookie per migliorare l’esperienza di navigazione degli utenti e per raccogliere informazioni sull’utilizzo del sito stesso. Proseguendo nella navigazione si accetta l’uso dei cookie; in caso contrario è possibile abbandonare il sito.</div>
            <div class="cli-bar-btn_container"><a href='http://blog.test/cookie-policy/' id="CONSTANT_OPEN_URL" target="_blank" class="cli-plugin-main-link" style="display:inline-block; margin:0px 10px 0px 5px; ">Cookie Policy</a><a role='button' tabindex='0' data-cli_action="accept" id="cookie_action_close_header" class="medium cli-plugin-button cli-plugin-main-button cookie_action_close_header cli_action_button" style="display:inline-block; ">Accetto</a></div>
        </div>
    </span></div>
<div id="cookie-law-info-again" style="display:none;" data-nosnippet="true"><span id="cookie_hdr_showagain">Manage consent</span></div>
<div class="cli-modal" data-nosnippet="true" id="cliSettingsPopup" tabindex="-1" role="dialog" aria-labelledby="cliSettingsPopup" aria-hidden="true">
    <div class="cli-modal-dialog" role="document">
        <div class="cli-modal-content cli-bar-popup">
            <button type="button" class="cli-modal-close" id="cliModalClose">
                <svg class="" viewBox="0 0 24 24">
                    <path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path>
                    <path d="M0 0h24v24h-24z" fill="none"></path>
                </svg>
                <span class="wt-cli-sr-only">Close</span>
            </button>
            <div class="cli-modal-body">
                <div class="cli-container-fluid cli-tab-container">
                    <div class="cli-row">
                        <div class="cli-col-12 cli-align-items-stretch cli-px-0">
                            <div class="cli-privacy-overview">
                                <h4>Privacy Overview</h4>
                                <div class="cli-privacy-content">
                                    <div class="cli-privacy-content-text">This website uses cookies to improve your experience while you navigate through the website. Out of these, the cookies that are categorized as necessary are stored on your browser as they are essential for the working of basic functionalities of the website. We also use third-party cookies that help us analyze and understand how you use this website. These cookies will be stored in your browser only with your consent. You also have the option to opt-out of these cookies. But opting out of some of these cookies may affect your browsing experience.</div>
                                </div>
                                <a class="cli-privacy-readmore" data-readmore-text="Show more" data-readless-text="Show less"></a>
                            </div>
                        </div>
                        <div class="cli-col-12 cli-align-items-stretch cli-px-0 cli-tab-section-container">

                            <div class="cli-tab-section">
                                <div class="cli-tab-header">
                                    <a role="button" tabindex="0" class="cli-nav-link cli-settings-mobile" data-target="necessary" data-toggle="cli-toggle-tab">
                                        Necessary </a>
                                    <div class="wt-cli-necessary-checkbox">
                                        <input type="checkbox" class="cli-user-preference-checkbox" id="wt-cli-checkbox-necessary" data-id="checkbox-necessary" checked="checked" />
                                        <label class="form-check-label" for="wt-cli-checkbox-necessary">Necessary</label>
                                    </div>
                                    <span class="cli-necessary-caption">Always Enabled</span>
                                </div>
                                <div class="cli-tab-content">
                                    <div class="cli-tab-pane cli-fade" data-id="necessary">
                                        <p>Necessary cookies are absolutely essential for the website to function properly. This category only includes cookies that ensures basic functionalities and security features of the website. These cookies do not store any personal information.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-tab-section">
                                <div class="cli-tab-header">
                                    <a role="button" tabindex="0" class="cli-nav-link cli-settings-mobile" data-target="non-necessary" data-toggle="cli-toggle-tab">
                                        Non-necessary </a>
                                    <div class="cli-switch">
                                        <input type="checkbox" id="wt-cli-checkbox-non-necessary" class="cli-user-preference-checkbox" data-id="checkbox-non-necessary" checked='checked' />
                                        <label for="wt-cli-checkbox-non-necessary" class="cli-slider" data-cli-enable="Enabled" data-cli-disable="Disabled"><span class="wt-cli-sr-only">Non-necessary</span></label>
                                    </div>
                                </div>
                                <div class="cli-tab-content">
                                    <div class="cli-tab-pane cli-fade" data-id="non-necessary">
                                        <p>Any cookies that may not be particularly necessary for the website to function and is used specifically to collect user personal data via analytics, ads, other embedded contents are termed as non-necessary cookies. It is mandatory to procure user consent prior to running these cookies on your website.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
<div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
<!--googleon: all-->
<div class="gdpr-overlay"></div>
<div class="gdpr gdpr-privacy-preferences">
    <div class="gdpr-wrapper">
        <form method="post" class="gdpr-privacy-preferences-frm" action="https://www.fulviobugani.com/wp-admin/admin-post.php">
            <input type="hidden" name="action" value="uncode_privacy_update_privacy_preferences">
            <input type="hidden" id="update-privacy-preferences-nonce" name="update-privacy-preferences-nonce" value="6360e636fc" /><input type="hidden" name="_wp_http_referer" value="/about-me/" />
            <header>
                <div class="gdpr-box-title">
                    <h3>Privacy Preference Center</h3>
                    <span class="gdpr-close"></span>
                </div>
            </header>
            <div class="gdpr-content">
                <div class="gdpr-tab-content">
                    <div class="gdpr-consent-management gdpr-active">
                        <header>
                            <h4>Privacy Preferences</h4>
                        </header>
                        <div class="gdpr-info">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
            <footer>
                <input type="submit" class="btn-accent btn-flat" value="Save Preferences">
            </footer>
        </form>
    </div>
</div>
<script type="text/javascript">
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");

        var x = document.getElementById("myDropdown2");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }

    }

    function myFunction2() {
        document.getElementById("hiddentext").classList.toggle("show");


    }
</script>
<script type="text/html" id="wpb-modifications"></script>
<script type='text/javascript' src='http://blog.test/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var DavesWordPressLiveSearchConfig = {
        "resultsDirection": "",
        "showThumbs": "false",
        "showExcerpt": "false",
        "displayPostCategory": "false",
        "showMoreResultsLink": "true",
        "activateWidgetLink": "true",
        "minCharsToSearch": "0",
        "xOffset": "0",
        "yOffset": "0",
        "blogURL": "https:\/\/www.fulviobugani.com",
        "ajaxURL": "https:\/\/www.fulviobugani.com\/wp-admin\/admin-ajax.php",
        "viewMoreText": "View more results",
        "outdatedJQuery": "Dave's WordPress Live Search requires jQuery 1.2.6 or higher. WordPress ships with current jQuery versions. But if you are seeing this message, it's likely that another plugin is including an earlier version.",
        "resultTemplate": "<ul id=\"dwls_search_results\" class=\"search_results dwls_search_results\" role=\"presentation\" aria-hidden=\"true\">\n<input type=\"hidden\" name=\"query\" value=\"<%- resultsSearchTerm %>\" \/>\n<% _.each(searchResults, function(searchResult, index, list) { %>\n        <%\n        \/\/ Thumbnails\n        if(DavesWordPressLiveSearchConfig.showThumbs == \"true\" && searchResult.attachment_thumbnail) {\n                liClass = \"post_with_thumb\";\n        }\n        else {\n                liClass = \"\";\n        }\n        %>\n        <li class=\"post-<%= searchResult.ID %> daves-wordpress-live-search_result <%- liClass %>\">\n\n        <a href=\"<%= searchResult.permalink %>\" class=\"daves-wordpress-live-search_title\">\n        <% if(DavesWordPressLiveSearchConfig.displayPostCategory == \"true\" && searchResult.post_category !== undefined) { %>\n                <span class=\"search-category\"><%= searchResult.post_category %><\/span>\n        <% } %><span class=\"search-title\"><%= searchResult.post_title %><\/span><\/a>\n\n        <% if(searchResult.post_price !== undefined) { %>\n                <p class=\"price\"><%- searchResult.post_price %><\/p>\n        <% } %>\n\n        <% if(DavesWordPressLiveSearchConfig.showExcerpt == \"true\" && searchResult.post_excerpt) { %>\n                <%= searchResult.post_excerpt %>\n        <% } %>\n\n        <% if(e.displayPostMeta) { %>\n                <p class=\"meta clearfix daves-wordpress-live-search_author\" id=\"daves-wordpress-live-search_author\">Posted by <%- searchResult.post_author_nicename %><\/p><p id=\"daves-wordpress-live-search_date\" class=\"meta clearfix daves-wordpress-live-search_date\"><%- searchResult.post_date %><\/p>\n        <% } %>\n        <div class=\"clearfix\"><\/div><\/li>\n<% }); %>\n\n<% if(searchResults[0].show_more !== undefined && searchResults[0].show_more && DavesWordPressLiveSearchConfig.showMoreResultsLink == \"true\") { %>\n        <div class=\"clearfix search_footer\"><a href=\"<%= DavesWordPressLiveSearchConfig.blogURL %>\/?s=<%-  resultsSearchTerm %>\"><%- DavesWordPressLiveSearchConfig.viewMoreText %><\/a><\/div>\n<% } %>\n\n<\/ul>\n"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='http://blog.test/wp-content/plugins/uncode-daves-wordpress-live-search/js/daves-wordpress-live-search.js?ver=5.4.15'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/www.fulviobugani.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    /* ]]> */
</script>
<script type='text/javascript' src='http://blog.test/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.9'></script>
<script type='text/javascript' src='http://blog.test/wp-content/plugins/uncode-privacy/assets/js/js-cookie.min.js?ver=2.2.0'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var Uncode_Privacy_Parameters = {
        "accent_color": "#0d669d"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='http://blog.test/wp-content/plugins/uncode-privacy/assets/js/uncode-privacy-public.min.js?ver=2.1.1'></script>
<script type='text/javascript'>
    var mejsL10n = {
        "language": "en",
        "strings": {
            "mejs.download-file": "Download File",
            "mejs.install-flash": "You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/",
            "mejs.fullscreen": "Fullscreen",
            "mejs.play": "Play",
            "mejs.pause": "Pause",
            "mejs.time-slider": "Time Slider",
            "mejs.time-help-text": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.",
            "mejs.live-broadcast": "Live Broadcast",
            "mejs.volume-help-text": "Use Up\/Down Arrow keys to increase or decrease volume.",
            "mejs.unmute": "Unmute",
            "mejs.mute": "Mute",
            "mejs.volume-slider": "Volume Slider",
            "mejs.video-player": "Video Player",
            "mejs.audio-player": "Audio Player",
            "mejs.captions-subtitles": "Captions\/Subtitles",
            "mejs.captions-chapters": "Chapters",
            "mejs.none": "None",
            "mejs.afrikaans": "Afrikaans",
            "mejs.albanian": "Albanian",
            "mejs.arabic": "Arabic",
            "mejs.belarusian": "Belarusian",
            "mejs.bulgarian": "Bulgarian",
            "mejs.catalan": "Catalan",
            "mejs.chinese": "Chinese",
            "mejs.chinese-simplified": "Chinese (Simplified)",
            "mejs.chinese-traditional": "Chinese (Traditional)",
            "mejs.croatian": "Croatian",
            "mejs.czech": "Czech",
            "mejs.danish": "Danish",
            "mejs.dutch": "Dutch",
            "mejs.english": "English",
            "mejs.estonian": "Estonian",
            "mejs.filipino": "Filipino",
            "mejs.finnish": "Finnish",
            "mejs.french": "French",
            "mejs.galician": "Galician",
            "mejs.german": "German",
            "mejs.greek": "Greek",
            "mejs.haitian-creole": "Haitian Creole",
            "mejs.hebrew": "Hebrew",
            "mejs.hindi": "Hindi",
            "mejs.hungarian": "Hungarian",
            "mejs.icelandic": "Icelandic",
            "mejs.indonesian": "Indonesian",
            "mejs.irish": "Irish",
            "mejs.italian": "Italian",
            "mejs.japanese": "Japanese",
            "mejs.korean": "Korean",
            "mejs.latvian": "Latvian",
            "mejs.lithuanian": "Lithuanian",
            "mejs.macedonian": "Macedonian",
            "mejs.malay": "Malay",
            "mejs.maltese": "Maltese",
            "mejs.norwegian": "Norwegian",
            "mejs.persian": "Persian",
            "mejs.polish": "Polish",
            "mejs.portuguese": "Portuguese",
            "mejs.romanian": "Romanian",
            "mejs.russian": "Russian",
            "mejs.serbian": "Serbian",
            "mejs.slovak": "Slovak",
            "mejs.slovenian": "Slovenian",
            "mejs.spanish": "Spanish",
            "mejs.swahili": "Swahili",
            "mejs.swedish": "Swedish",
            "mejs.tagalog": "Tagalog",
            "mejs.thai": "Thai",
            "mejs.turkish": "Turkish",
            "mejs.ukrainian": "Ukrainian",
            "mejs.vietnamese": "Vietnamese",
            "mejs.welsh": "Welsh",
            "mejs.yiddish": "Yiddish"
        }
    };
</script>
<script type='text/javascript' src='http://blog.test/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.13-9993131'></script>
<script type='text/javascript' src='http://blog.test/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.4.15'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpmejsSettings = {
        "pluginPath": "\/wp-includes\/js\/mediaelement\/",
        "classPrefix": "mejs-",
        "stretching": "responsive"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='http://blog.test/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.4.15'></script>
<script type='text/javascript' src='http://blog.test/wp-content/themes/blog/uncode/library/js/plugins.js?ver=1439317685'></script>
<script type='text/javascript' src='http://blog.test/wp-content/themes/blog/uncode/library/js/app.js?ver=1439317685'></script>
<script type='text/javascript' src='http://blog.test/wp-includes/js/wp-embed.min.js?ver=5.4.15'></script>

<?php
get_footer();
?>