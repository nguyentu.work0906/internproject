<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the website, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Blog' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4AVDDtEQk1qQBXco3Ef5pahJ679tVxxcJeXy7dCV7nmKiGvvS3kPV2FRj5vCoF47' );
define( 'SECURE_AUTH_KEY',  'M92GYW6SKstkrNbWtHEcL0fUNjcnWVmo88x1JeVLDQP1tm10wN8v3C3GPB6IinKy' );
define( 'LOGGED_IN_KEY',    'ZpECV42ykpfiX9quHebpLaOoPlL2I5wYSfSb2e0G8AX7ZNuSaZwyzpnQqdKacwgX' );
define( 'NONCE_KEY',        'glXGFsDD2GOs2U0hOAQm5WxBdESPnC1ZtkCo77KPB48mkLJQHRezVV6HvGFdu8xC' );
define( 'AUTH_SALT',        'vrUG1dpwEiY1jsDsuwWWdPGWpOe4BXOiEW06M2aR70M3gX6GhTh7m68dlJWs6ZrD' );
define( 'SECURE_AUTH_SALT', '1bCfy0W6I0hlKLYCLJstxHx8X6PUrVYp9qkVDt8RB8BiwhsumIxr70Fy9eb3bDqH' );
define( 'LOGGED_IN_SALT',   'OeJv5zTKjzvCUgF2yZwIkjhSaHdzOVNLKMwUrj159EDfWfAuP3gxXP6ro5Dv9wh3' );
define( 'NONCE_SALT',       'UJUpbNqjoUhgZJs4oVyhrNjkSyDqhJDE5uzxNBDmGplm87BzJ1PT2MIwRMD6hUB2' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
